const express = require("express");
const http = require("http");
const socketIo = require("socket.io");

const port = process.env.PORT || 4001;
const index = require("./routes/index");
var UserController = require('./routes/user/UserController');
var UserInforController = require('./routes/userinfor/UserInforController');

var db = require('./db')

var members = require('./gamecharacters');
var selectorcount = require('./gameselectors');
var visibility = require('./visibility');
var varify = require('./routes/verification/verification');
var cors = require('cors');

const path = require("path");

var gamerooms = [];



const app = express();
app.use(cors());
//app.use(index);
app.use(express.static(path.join(__dirname, "client/build")))

app.get('/game', (request, response) => {
  response.sendFile(path.join(__dirname, 'client/build', 'index.html'));
});
app.get('/game/*', (request, response) => {
  response.sendFile(path.join(__dirname, 'client/build', 'index.html'));
});
app.get('/Registration', (request, response) => {
  response.sendFile(path.join(__dirname, 'client/build', 'index.html'));
});
app.get('/Registration/*', (request, response) => {
  response.sendFile(path.join(__dirname, 'client/build', 'index.html'));
});
app.get('/login', (request, response) => {
  response.sendFile(path.join(__dirname, 'client/build', 'index.html'));
});
app.get('/login/*', (request, response) => {
  response.sendFile(path.join(__dirname, 'client/build', 'index.html'));
});


app.get('/verification/*', function (req, res) {
  //verification/?user=123&option=456

  var query = require('url').parse(req.url, true).query;
  // get url parameters
  const userid = query.user;
  const type = query.option;

  varify.verify(userid);
  res.sendFile(path.join(__dirname, 'client/build', 'index.html'));
});


const server = http.createServer(app);
const io = socketIo(server);
let interval;



app.use('/users', UserController);
app.use('/userinfor', UserInforController);


io.on("connection", (socket) => {
  console.log("New client connected");
  console.log("connected : " + socket.id);
  if (interval) {
    clearInterval(interval);
  }

  socket.on("disconnect", () => {
    console.log("Client disconnected");
    console.log('User disconnected. Socket id %s', socket.id);
    //console.log(io.sockets.sockets[socket.id]);

    clearInterval(interval);
  });

  //**************************************************************************************************** */

  io.on('connection', function (socket) {

  });


  socket.on('CreateGameRoom', function (data) {//to create new room
    var thisGameId = 0;
    do {
      var ids = (Math.random() * 100000) | 0;
      var ind = gamerooms.indexOf(parseInt(ids));
      if (ind == -1) {
        thisGameId = ids;
        console.log("get new id")
      } else {
        thisGameId = 0;
      }
    }
    while (thisGameId < 0);

    console.log("new game createed", thisGameId);
    this.emit('CratedRoomID', { gameId: thisGameId, mySocketId: this.id });

    var gammerdata = { email: data.email, name: data.name, socketid: this.id, room_id: data.room_id }

    global['gameroom' + thisGameId] = GameLogic();
    global['gameroom' + thisGameId].addgammer(gammerdata);

    gamerooms.push(thisGameId)

    // Join the Room and wait for the players
    this.join(thisGameId.toString());

    //this["gameroom" + thisGameId].setMessage("ok");
  });



  //************************************************************************************************* */
  socket.on('JointoRoom', function (data) {//To join room

    var room_exit = gamerooms.indexOf(parseInt(data.room_id));

    if (room_exit != -1) {
      if (global['gameroom' + data.room_id].getcoutOfGammers() >= 12) {

        var err = { error: "You Cant Join to game", error_code: 0003 }
        io.to(socket.id).emit('Error', err);

      } else {
        socket.join(data.room_id);

        var gammerdata = { email: data.email, name: data.name, socketid: socket.id, room_id: data.room_id }

        global['gameroom' + data.room_id].addgammer(gammerdata);

        console.log('User joined to room', data.room_id);

        io.in(data.room_id).clients((err, clients) => {
          console.log(clients); // an array containing socket ids in 'room3'
        });
        //socket.to(room).emit('newGameCreated', { gameId: '14', mySocketId: '444'});
        io.in(data.room_id).emit('JoinedGame', { gameId: '14', mySocketId: 'name' });//for send all users in game room 

      }

    } else {
      console.log("room id not exit");
      var err = { error: "Room does not Exit !!", error_code: 0001 }
      io.to(socket.id).emit('Error', err);

    }

  });


  socket.on('StartGame', function (data) {//start to game
    global['gameroom' + data.room_id].startgame(data.gamedata);


  });
  socket.on('SelectByLeader', function (data) {//
    global['gameroom' + data.room_id].selectByLeader(data.selectedemail);
    //console.log("select",data.room_id)
  });

  socket.on('RequestToAproval', function (data) {//for request aproval by leader, data={"room_id":'1234'}
    global['gameroom' + data.room_id].requiretoaproveal();
  });

  socket.on('SubmitAproval', function (data) {//for submit aproval by leader, data={"room_id","email":'',"ans":true}
    global['gameroom' + data.room_id].submitaproval(data);
  });

  socket.on('SubmitSuccessOrFail', function (data) {//for submit success or fail by selected person, data={"room_id","email":'',"ans":true}
    global['gameroom' + data.room_id].submitRequestOfSuccessOrFails(data);
  });

  socket.on('ClickGuestMerlin', function (data) {//for click merlin, data={"room_id","selectedemail":''}
    global['gameroom' + data.room_id].clickingGuestMerlin(data.selectedemail);
  });
  socket.on('SubmitGuestMerlin', function (data) {//for submit merlin geust data={"room_id","email":'',"ans":"isha@gmail.com"}
    global['gameroom' + data.room_id].submitGuestedMerlin(data);
  });

  socket.on('SetQuest', function (data) {//for set quest data={"room_id","quest":''}
    global['gameroom' + data.room_id].setQuest(data.quest);
  });

  socket.on('SubmitQuest', function (data) {//for submit quest data={"room_id"}
    global['gameroom' + data.room_id].submitQuest();
  });

  socket.on('StartNewGame', function (data) {//for submit quest data={"room_id"}
    global['gameroom' + data.room_id].startNewGame();
  });

  socket.on('RequestByLakeLady', function (data) {//for submit quest data={"room_id"}
    global['gameroom' + data.room_id].requestLakelady(data);
  });

  socket.on('Acceptlady', function (data) {//for submit quest data={"room_id"}
    io.in(data.room_id).emit('hideladymodel');//for send all users in game room 

    if (data.ans) {
      global['gameroom' + data.room_id].ladyLakeselecting(data);
    }
  });


  socket.on('LogOut', function (data) {//for logout data={"room_id","email"}

    var room_exit = gamerooms.indexOf(parseInt(data.room_id));

    if (room_exit != -1) {
      global['gameroom' + data.room_id].logout(data);
    }

  });

  socket.on('CallLink', function (data) {//for logout data={"room_id","email"}

    global['gameroom' + data.room_id].setCallLink(data.text);


  });


});


var minutes = 120;
var the_interval = minutes * 60 * 1000;
//var the_interval = 5000;
setInterval(function () {
  // do your stuff here

  for (var i = 0; i < gamerooms.length; i++) {
    var ii = gamerooms[i];
    if (ii != undefined) {
      global['gameroom' + ii].checkclientsstatus();
      console.log("checking room is online or not :", gamerooms[i])
    }
  }

}, the_interval);


function deleteobject(id) {
  console.log("game room", gamerooms);
  console.log("deleted room", id)
  var ind = gamerooms.indexOf(parseInt(id));
  gamerooms.splice(ind, 1);
  console.log("game room", gamerooms);
}


function GameLogic() {//for game logic for each room
  var callLink = '';
  var roomLock = false;
  var room_id = '';
  var gamestatus = false;
  var gamers_name = [];
  var gamers_email = [];
  var gamers_socketid = [];
  var gamers_character = [];
  var leader = [];
  var lady_lake = [];
  var lady_request_count = 0;
  var selectLeader = [];
  var sheild = [];
  var guestedMerlin = [];
  var guestperson = [];
  var goodVsEvil = {};
  var requered_selectors_in_game = [{ count: 2, evil_count: 1 }, { count: 3, evil_count: 1 }, { count: 2, evil_count: 1 }, { count: 3, evil_count: 1 }, { count: 3, evil_count: 1 }];
  var requered_selectors = 1;//need to set 
  var current_quest = 1;//need to up next level
  var requered_evils_number = 1;//need to set up each level
  var anonymous_approve = false;
  var goodGuysCanFailQuests = false;
  var lady_of_the_Lake = true;
  var current_evil_on_fail = 0;//submited fails evils 
  var submited_count = 0;//for all success,fails count
  var aprovals = [];
  var aprovals_temp = [];
  var currentleader = -1;
  var currentLady = -1;
  var rejected_count = 0;
  var quest = ["unknown", "unknown", "unknown", "unknown", "unknown"];
  var callaccept = false;
  var callSuccessFail = false;
  var successfails = [];
  var getting_lady = false;
  var onlinestatus = [];
  var good = [];
  var evil = [];
  var lady_selector = [];
  var gameEnd = false;
  var won = 'none';
  var selecting = false;
  var questcombination = [{ total: '', fail_count: '' }, { total: '', fail_count: '' }, { total: '', fail_count: '' }, { total: '', fail_count: '' }, { total: '', fail_count: '' }];
  var currentcombination = {};
  var privius_lady = [];
  var startagainquest=false;

  var interval = setInterval(function () {

    for (var i = 0; i < gamers_email.length; i++) {

      if (io.sockets.connected[gamers_socketid[i]] != undefined) {
        onlinestatus[i] = (io.sockets.connected[gamers_socketid[i]].connected); //or disconected property
      } else {
        onlinestatus[i] = false;
      }
    }

    getgamedata();
  }, 6000);


  function logout(data) {
    var ind = gamers_email.indexOf(data.email);
    console.log("logout", ind)

    if (ind > -1) {

      gamers_email.splice(ind, 1);
      gamers_name.splice(ind, 1);
      gamers_socketid.splice(ind, 1);
      gamers_character.splice(ind, 1);
      leader.splice(ind, 1);
      lady_lake.splice(ind, 1);
      selectLeader.splice(ind, 1);
      sheild.splice(ind, 1);
      aprovals.splice(ind, 1);
      aprovals_temp.splice(ind, 1);
      guestedMerlin.splice(ind, 1);
      guestperson.splice(ind, 1);
      successfails.splice(ind, 1);
      onlinestatus.splice(ind, 1);
    }
    console.log("logout end", gamers_email)

    getgamedata();
  }



  function destroy() {
    clearInterval(interval);
    deleteobject(room_id);
    callLink = '';
    room_id = '';
    gamestatus = false;
    gamers_name = [];
    gamers_email = [];
    gamers_socketid = [];
    gamers_character = [];
    leader = [];
    lady_lake = [];
    lady_request_count = 0;
    selectLeader = [];
    sheild = [];
    guestedMerlin = [];
    guestperson = [];
    goodVsEvil = {};
    requered_selectors_in_game = [{ count: 2, evil_count: 1 }, { count: 3, evil_count: 1 }, { count: 2, evil_count: 1 }, { count: 3, evil_count: 1 }, { count: 3, evil_count: 1 }];
    requered_selectors = 1;//need to set 
    current_quest = 1;//need to up next level
    requered_evils_number = 1;//need to set up each level
    anonymous_approve = false;
    goodGuysCanFailQuests = false;
    lady_of_the_Lake = true;
    current_evil_on_fail = 0;//submited fails evils 
    submited_count = 0;//for all success,fails count
    aprovals = [];
    aprovals_temp = [];
    currentleader = -1;
    currentLady = -1;
    rejected_count = 0;
    quest = ["unknown", "unknown", "unknown", "unknown", "unknown"];
    callaccept = false;
    callSuccessFail = false;
    successfails = [];
    getting_lady = false;
    onlinestatus = [];
    good = [];
    evil = [];
    lady_selector = [];
    questcombination = [{ total: '', fail_count: '' }, { total: '', fail_count: '' }, { total: '', fail_count: '' }, { total: '', fail_count: '' }, { total: '', fail_count: '' }];
    privius_lady = [];
  }


  function checkclientsstatus() {

    var oflinecount = 0;

    for (var i = 0; i < gamers_email.length; i++) {

      if (io.sockets.connected[gamers_socketid[i]] != undefined) {
        //console.log("socket status", io.sockets.connected[gamers_socketid[i]].connected); //or disconected property

      } else {
        oflinecount++;
        console.log("Socket not connected");
      }
    }
    if (oflinecount == gamers_email.length) {
      console.log("can delete")
      destroy();
    }

  }


  function getcoutOfGammers() {
    return gamers_email.length;
  }


  function addgammer(data) {//Add new gamer to room

    if (gamers_email.indexOf(data.email) === -1 && roomLock == false) {
      gamers_email.push(data.email);
      gamers_name.push(data.name);
      gamers_socketid.push(data.socketid);
      gamers_character.push("unknown");
      leader.push(false);
      lady_lake.push(false);
      selectLeader.push(false);
      sheild.push(false);
      aprovals.push("unknown");
      aprovals_temp.push("unknown");
      guestedMerlin.push("unknown");
      guestperson.push(false);
      room_id = data.room_id;
      successfails.push("unknown");
      onlinestatus.push(true),
        lady_selector.push("unknown");
    }
    else {
      gamers_socketid[gamers_email.indexOf(data.email)] = data.socketid;
      //console.log("element found");
    }
    //console.log(gamers_socketid);
    getgamedata();
  }

  //********************************************************************************************************* */

  function creategamedata(email) {

    var gameobject = [];

    var from = gamers_email.indexOf(email);


    if (gameEnd) {
      for (var i = 0; i < gamers_email.length; i++) {
        if (i + from >= gamers_email.length) {
          //console.log(gamers_email[(i + from) - gamers_email.length]);
          var visi = visibility.getvisibility(gamers_character[(i + from) - gamers_email.length], gamers_character[(i + from) - gamers_email.length])
          gameobject.push(
            {
              'name': gamers_name[(i + from) - gamers_email.length],
              'email': gamers_email[(i + from) - gamers_email.length],
              'socketid': gamers_socketid[(i + from) - gamers_email.length],
              'current_quest': current_quest,
              'quest': quest,
              'gamestatus': gamestatus,
              'character': gamers_character[(i + from) - gamers_email.length],
              'goodvsevil': goodVsEvil,
              'aproval': aprovals_temp[(i + from) - gamers_email.length],
              'type': visi.type,
              'leader': false,
              'lady_lake': false,
              'selectByLeader': false,
              'sheild': false,
              'gestedMerlin': guestedMerlin[(i + from) - gamers_email.length],
              'guestperson': guestperson[(i + from) - gamers_email.length],
              'req': requered_selectors_in_game,
              'wonby': 'none',
              'rejected_count': rejected_count,
              //'accept_arry': aprovals[(i + from) - gamers_email.length],
              //'callaccept': callaccept,
              //'callSuccessFail': callSuccessFail,
              //'successfails': successfails[(i + from) - gamers_email.length],
              'onlinestatus': onlinestatus[(i + from) - gamers_email.length],
              'good_characters': good,//*
              'evil_caracters': evil,//*
              'calllink': callLink,
              'lady_select': "unknown",
              'selecting': selecting,
              'questcombination': questcombination,
              'endgame': gameEnd

            });
        } else {
          //console.log(gamers_email[i + from]);
          var visi = visibility.getvisibility(gamers_character[i + from], gamers_character[i + from])

          gameobject.push(
            {
              'name': gamers_name[i + from],
              'email': gamers_email[i + from],
              'socketid': gamers_socketid[i + from],
              'current_quest': current_quest,
              'quest': quest,
              'gamestatus': gamestatus,
              'character': gamers_character[i + from],
              'goodvsevil': goodVsEvil,
              'aproval': aprovals_temp[i + from],
              'type': visi.type,
              'leader': false,
              'lady_lake': false,
              'selectByLeader': false,
              'sheild': false,
              'gestedMerlin': guestedMerlin[i + from],
              'guestperson': guestperson[i + from],
              'req': requered_selectors_in_game,
              'wonby': 'none',
              'rejected_count': rejected_count,
              //'accept_arry': aprovals[(i + from) - gamers_email.length],
              //'callaccept': callaccept,
              //'callSuccessFail': callSuccessFail,
              //'successfails': successfails[(i + from) - gamers_email.length],
              'onlinestatus': onlinestatus[(i + from)],
              'good_characters': good,//*
              'evil_caracters': evil,//*
              'calllink': callLink,
              'lady_select': "unkonwn",
              'selecting': selecting,
              'questcombination': questcombination,
              'endgame': gameEnd
            }
          );
        }
      }


    } else {

      for (var i = 0; i < gamers_email.length; i++) {

        if (i + from >= gamers_email.length) {
          //console.log(gamers_email[(i + from) - gamers_email.length]);
          var visi = visibility.getvisibility(gamers_character[from], gamers_character[(i + from) - gamers_email.length])
          var lady_selected_character = "unknown";

          if (lady_lake[from]) {
            lady_selected_character = lady_selector[(i + from) - gamers_email.length];
          } else if (lady_selector[(i + from) - gamers_email.length] != "unknown") {
            lady_selected_character = true;
          }

          gameobject.push(
            {
              'name': gamers_name[(i + from) - gamers_email.length],
              'email': gamers_email[(i + from) - gamers_email.length],
              'socketid': gamers_socketid[(i + from) - gamers_email.length],
              'current_quest': current_quest,
              'quest': quest,
              'gamestatus': gamestatus,
              'character': visi.character,
              'goodvsevil': goodVsEvil,
              'aproval': aprovals_temp[(i + from) - gamers_email.length],
              'type': visi.type,
              'leader': leader[(i + from) - gamers_email.length],
              'lady_lake': lady_lake[(i + from) - gamers_email.length],
              'selectByLeader': selectLeader[(i + from) - gamers_email.length],
              'sheild': sheild[(i + from) - gamers_email.length],
              'gestedMerlin': guestedMerlin[(i + from) - gamers_email.length],
              'guestperson': guestperson[(i + from) - gamers_email.length],
              'req': requered_selectors_in_game,
              'wonby': 'none',
              'rejected_count': rejected_count,

              'accept_arry': aprovals[(i + from) - gamers_email.length],
              'callaccept': callaccept,
              'callSuccessFail': callSuccessFail,
              'successfails': successfails[(i + from) - gamers_email.length],
              'onlinestatus': onlinestatus[(i + from) - gamers_email.length],
              'good_characters': good,
              'evil_caracters': evil,
              'calllink': callLink,
              'lady_select': lady_selected_character,
              'selecting': selecting,
              'questcombination': questcombination,
              'endgame': gameEnd
            });
        } else {
          //console.log(gamers_email[i + from]);
          var visi = visibility.getvisibility(gamers_character[from], gamers_character[i + from])
          var lady_selected_character = "unknown";

          if (lady_lake[from]) {
            lady_selected_character = lady_selector[(i + from)];
          } else if (lady_selector[(i + from)] != "unknown") {
            lady_selected_character = true;
          }
          gameobject.push(
            {
              'name': gamers_name[i + from],
              'email': gamers_email[i + from],
              'socketid': gamers_socketid[i + from],
              'current_quest': current_quest,
              'quest': quest,
              'gamestatus': gamestatus,
              'character': visi.character,
              'goodvsevil': goodVsEvil,
              'aproval': aprovals_temp[i + from],
              'type': visi.type,
              'leader': leader[i + from],
              'lady_lake': lady_lake[i + from],
              'selectByLeader': selectLeader[i + from],
              'sheild': sheild[i + from],
              'gestedMerlin': guestedMerlin[i + from],
              'guestperson': guestperson[i + from],
              'req': requered_selectors_in_game,
              'wonby': 'none',
              'rejected_count': rejected_count,

              'accept_arry': aprovals[(i + from)],
              'callaccept': callaccept,
              'callSuccessFail': callSuccessFail,
              'successfails': successfails[(i + from)],
              'onlinestatus': onlinestatus[(i + from)],
              'good_characters': good,
              'evil_caracters': evil,
              'calllink': callLink,
              'lady_select': lady_selected_character,
              'selecting': selecting,
              'questcombination': questcombination,
              'endgame': gameEnd
            }
          );
        }
      }

    }
    //console.log("game data", gameobject)
    return gameobject;

  }

  function getgamedata() {

    for (var i = 0; i < gamers_email.length; i++) {
      // creategamedata(gamers_email[i])
      io.to(gamers_socketid[i]).emit('gamedata', creategamedata(gamers_email[i]));
    }
    io.to(gamers_socketid[0]).emit('gamecreator', true);
  }

  function startgame(data) {

    if (gamers_email.length >= 5) {
      setQuest(current_quest);
      gameEnd = false;
      roomLock = true;
      gamestatus = true;
      leader = leader.map(x => false);
      lady_lake = lady_lake.map(x => false);
      selectLeader = selectLeader.map(x => false);
      sheild = sheild.map(x => false);
      guestedMerlin = guestedMerlin.map(x => "unknown");
      guestperson = guestperson.map(x => false);
      aprovals = aprovals.map(x => "unknown");
      aprovals_temp = aprovals_temp.map(x => "unknown");
      lady_selector = lady_selector.map(x => "unknown");
      quest = ["unknown", "unknown", "unknown", "unknown", "unknown"];
      currentleader = -1;
      submited_count = 0;
      anonymous_approve = data.anonymous_approve;
      goodGuysCanFailQuests = data.goodGuysCanFailQuests;
      lady_of_the_Lake = data.ladyOFTheLake;

      var allgamedata = members.getcharacters(gamers_email.length, data);
      var all_character = allgamedata.allcharacters;
      goodVsEvil = allgamedata.goodVsEvil;

      for (var i = 0; i < gamers_email.length; i++) {

        var ind = Math.floor(Math.random() * all_character.length);
        gamers_character[i] = all_character[ind];
        all_character.splice(ind, 1);
      }
      //console.log(gamers_character);

      setLeader();
      getallcharacters();
      getgamedata();
    } else {
      var err = { error: "Need more than 5 gamers to start", error_code: 0002 }
      io.to(gamers_socketid[0]).emit('Error', err);
    }

  }


  function setLeader() {//for set leader by randomly
    selecting = true;
    if (currentleader == -1) {

      currentleader = Math.floor(Math.random() * gamers_email.length);
      leader[currentleader] = true;
      currentLady = currentleader + 1;
      if (currentleader >= leader.length - 1) {
        currentLady = 0;
      }
    } else {
      currentleader = currentleader + 1;
      if (currentleader >= leader.length) {
        leader = leader.map(x => false);
        leader[currentleader % leader.length] = true;
      } else {
        leader = leader.map(x => false);
        leader[currentleader] = true;
      }

      if (startagainquest == false) {
        currentLady = currentLady - 1;
        if (currentLady == -1) {
          currentLady = lady_lake.length - 1
        }
      }


    }

    lady_selector = lady_selector.map(x => "unknown");
    lady_lake = lady_lake.map(x => false);
    var gamerounds = count(quest, "unknown");

    var lady = false;

    if (gamerounds <= 3 && gamerounds >= 1) {
      lady = true;
    }

    if (lady_of_the_Lake && lady && startagainquest == false) {
      lady_request_count = 0;
      lady_lake = lady_lake.map(x => false);

      lady_lake[currentLady] = true;
      privius_lady.push(gamers_email[currentLady]);

      getgamedata();
      //io.to(gamers_socketid[0]).emit('Ask_About_lady_lake');
      // io.in(room_id).emit('Ask_About_lady_lake');//

    }


  }

  function ladyLakeselecting() {
    lady_lake[currentLady] = true;
    getgamedata();
  }



  function selectByLeader(email) {//Select persons for quest by leader

    if (lady_lake[currentLady] && lady_request_count == 0) {
      console.log("requed lady of the lake action")
      var dat = { error: 'Lady of the Lake action is required' }
      // io.in(room_id).emit('ErrorModel', dat);//for send all users in game room 

      var err = { error: "Lady of the Lake action is required", error_code: 0005 }
      //io.to(gamers_socketid[0]).emit('Error', err);
      io.in(room_id).emit('Error', err);

    } else {

      var i = gamers_email.indexOf(email);
      var selectednumber = selectLeader.filter(Boolean).length;

      if (selectednumber < requered_selectors) {
        selectLeader[i] = !selectLeader[i];
        getgamedata();
      } else if ((selectednumber == requered_selectors) && (selectLeader[i] == true)) {
        selectLeader[i] = !selectLeader[i];
        getgamedata();
      }

      console.log("selected", gamers_email);
    }

  }


  function requiretoaproveal() {//after finish selecting by leader
    var selectednumber = selectLeader.filter(Boolean).length;

    if (selectednumber == requered_selectors) {

      selecting = false;
      callaccept = true;
      getgamedata();
      console.log("sended reqest for aproval");

    }

  }



  function submitaproval(data) {// for aproval data={"email":"isha@gmail.com","ans":true}

    if (aprovals[gamers_email.indexOf(data.email)] === "unknown") {
      aprovals[gamers_email.indexOf(data.email)] = data.ans;
      //console.log("approvals", aprovals);
    }

    getgamedata();

    setTimeout(function () {
      if (count(aprovals, "unknown") === 0) {
        callaccept = false;

        var approvecount = count(aprovals, true);
        var unapprovecount = count(aprovals, false);

        if (!anonymous_approve) { //For visible approval persons
          aprovals_temp = aprovals;
        }

        if (unapprovecount >= approvecount) {
          restartquest();
        } else {
          sendRequestOfSuccessOrFails();
        }

      }
    }, 1000);


  }

  function sendRequestOfSuccessOrFails() {
    selectLeader.map((value, index) => {
      if (value) {
        sheild[index] = true;
      }
    });
    getgamedata();

    setTimeout(function () {

      callSuccessFail = true;
      getgamedata();
    }, 3000);

  }

  var successfailqueue = [];
  function submitRequestOfSuccessOrFails(data) {// for submit success or fails data={"email":"isha@gmail.com","ans":true}

    if (successfails[gamers_email.indexOf(data.email)] === "unknown") {
      successfails[gamers_email.indexOf(data.email)] = data.ans;
      successfailqueue.push({ email: data.email, ans: data.ans });
    }
    getgamedata();

    setTimeout(function () {
      var current_success_count = 0;
      if (successfailqueue.length == count(selectLeader, true)) {
        console.log("complete submited of success faill")

        for (var j = 0; j < successfailqueue.length; j++) {
          var i = gamers_email.indexOf(successfailqueue[j].email);
          var type = visibility.getvisibility(gamers_character[i], gamers_character[i]).type;
          var ans = successfailqueue[j].ans;

          if (type == "evil" && ans == false) {
            current_evil_on_fail = current_evil_on_fail + 1;
          } else if (type == "good" && ans == false && goodGuysCanFailQuests) {
            current_evil_on_fail = current_evil_on_fail + 1;
          }


        }//end for loop

        callSuccessFail = false;
        successfails = successfails.map(x => "unknown");

        var dat = { total: successfailqueue.length, fail_count: current_evil_on_fail }
        currentcombination = dat;

        if (successfailqueue.length != 0) {
          io.in(room_id).emit('FailsCount', dat);//for send all users in game room 
        }
        console.log("success fails counts", dat)

        if ((current_evil_on_fail >= requered_evils_number) && (successfailqueue.length != 0)) {
          setWinningTeam("evil");
        } else if ((current_evil_on_fail < requered_evils_number) && (successfailqueue.length != 0)) {
          setWinningTeam("good");
        }

        successfailqueue = [];

      }

    }, 1000);

  }

  function setWinningTeam(team) {//set quest wininig team


    quest[current_quest - 1] = team;
    questcombination[current_quest - 1] = currentcombination;

    sheild = sheild.map(x => false);
    selectLeader = selectLeader.map(x => false);

    var quest_to_do = quest.indexOf("unknown");
    var won = false;

    getgamedata();

    if (count(quest, "good") == 3) {
      won = true;
      setTimeout(function () {
        aprovals = aprovals.map(x => 'unknown');//for clear aprovals
        aprovals_temp = aprovals_temp.map(x => 'unknown');
        leader = leader.map(x => false);

        getgamedata();
        for (var i = 0; i < gamers_email.length; i++) {
          // creategamedata(gamers_email[i])
          io.to(gamers_socketid[i]).emit('winby', "good");
        }
        sendRequestToguesMerlin();
      }, 3000);


    } else if (count(quest, "evil") == 3) {
      won = true;
      aprovals = aprovals.map(x => 'unknown');//for clear aprovals
      endgame("evil");

    } else if (quest_to_do != -1 && !won) {
      setTimeout(function () {
        gotoNextQuest();
      }, 3000);
    }

  }


  function sendRequestToguesMerlin() {
    var i = -1;
    if (gamers_character.indexOf("morgana") > -1) {
      i = gamers_character.indexOf("morgana");

    } else if (gamers_character.indexOf("assassin") > -1) {
      i = gamers_character.indexOf("assassin");

    } else if (gamers_character.indexOf("mordred") > -1) {
      i = gamers_character.indexOf("mordred");

    } else if (gamers_character.indexOf("oberon") > -1) {
      i = gamers_character.indexOf("oberon");
    }

    leader = leader.map(x => false);
    lady_selector = lady_selector.map(x => "unknown");
    sheild = sheild.map(x => false);
    selectLeader = selectLeader.map(x => false);
    lady_lake = lady_lake.map(x => false);

    guestperson[i] = true;

    getgamedata();
    io.to(gamers_socketid[i]).emit('GestMerlin');//send reqest to gess merlin

  }


  function clickingGuestMerlin(email) {

    guestedMerlin = guestedMerlin.map(x => 'unknown');//for clear gested merlin
    guestedMerlin[gamers_email.indexOf(email)] = true;
    getgamedata();
  }

  function submitGuestedMerlin(data) { //data={"room_id","email":'',"ans":"isha@gmail.com"}
    guestperson = guestperson.map(x => false);
    var guestedMerlinIndex = guestedMerlin.indexOf(true);

    if (gamers_character[guestedMerlinIndex] == "merlin") {

      endgame("evil");
    } else {

      endgame("good");
    }

  }


  function setQuest(i) {
    current_quest = i;
    requered_selectors_in_game = selectorcount.getselectorcount(gamers_email.length);

    requered_selectors = requered_selectors_in_game[i - 1].count;
    requered_evils_number = requered_selectors_in_game[i - 1].evil_count;

    getgamedata();
  }

  function submitQuest() {
    gamestatus = true;
    setLeader();
    getgamedata();
  }

  function gotoNextQuest() {// For go to next quest

    startagainquest = false;
    leader = leader.map(x => false);
    lady_selector = lady_selector.map(x => "unknown");
    sheild = sheild.map(x => false);
    selectLeader = selectLeader.map(x => false);
    rejected_count = 0;

    gamestatus = true;
    setLeader();
    //getgamedata();


    requered_selectors = 1;//need to set 
    requered_evils_number = 1;//need to set up each level

    current_evil_on_fail = 0;//submited fails evils 
    submited_count = 0;//for all success,fails count

    var qq = quest.indexOf("unknown");
    current_quest = qq + 1;

    if (qq != -1) {
      setQuest(current_quest);
    }

    //setLeader();

    setTimeout(function () {
      aprovals = aprovals.map(x => 'unknown');
      aprovals_temp = aprovals_temp.map(x => 'unknown');
      getgamedata();
    }, 3000);

  }

  function endgame(won) { //End game, Set winning team 


    for (var i = 0; i < gamers_email.length; i++) {
      // creategamedata(gamers_email[i])
      io.to(gamers_socketid[i]).emit('gamedata', crateGameDataInEnd(gamers_email[i], won));
    }

    setTimeout(function () {
      aprovals = aprovals.map(x => 'unknown');//for clear aprovals
      data = { "wonby": won };


      io.to(gamers_socketid[0]).emit('gamefinishAndRequest', data);

      for (var i = 1; i < gamers_email.length; i++) {
        // creategamedata(gamers_email[i])
        io.to(gamers_socketid[i]).emit('gamefinish', data);
      }
      current_quest = 1;
    }, 5000);


    roomLock = false;


  }

  function crateGameDataInEnd(email, wonby) {// Creating game data in end
    gameEnd = true;
    var gameobject = [];

    var from = gamers_email.indexOf(email);

    for (var i = 0; i < gamers_email.length; i++) {


      if (i + from >= gamers_email.length) {
        //console.log(gamers_email[(i + from) - gamers_email.length]);
        var visi = visibility.getvisibility(gamers_character[(i + from) - gamers_email.length], gamers_character[(i + from) - gamers_email.length])
        gameobject.push(
          {
            'name': gamers_name[(i + from) - gamers_email.length],
            'email': gamers_email[(i + from) - gamers_email.length],
            'socketid': gamers_socketid[(i + from) - gamers_email.length],
            'current_quest': current_quest,
            'quest': quest,
            'gamestatus': gamestatus,
            'character': gamers_character[(i + from) - gamers_email.length],
            'goodvsevil': goodVsEvil,
            'aproval': aprovals_temp[(i + from) - gamers_email.length],
            'type': visi.type,
            'leader': false,
            'lady_lake': false,
            'selectByLeader': false,
            'sheild': false,
            'gestedMerlin': guestedMerlin[(i + from) - gamers_email.length],
            'guestperson': guestperson[(i + from) - gamers_email.length],
            'req': requered_selectors_in_game,
            'wonby': wonby,
            'rejected_count': rejected_count,
            //'accept_arry': aprovals[(i + from) - gamers_email.length],
            //'callaccept': callaccept,
            //'callSuccessFail': callSuccessFail,
            //'successfails': successfails[(i + from) - gamers_email.length],
            'onlinestatus': onlinestatus[(i + from) - gamers_email.length],
            'good_characters': good,//*
            'evil_caracters': evil,//*
            'calllink': callLink,
            'lady_select': "unknown",
            'questcombination': questcombination,
            'endgame': gameEnd
          });
      } else {
        //console.log(gamers_email[i + from]);
        var visi = visibility.getvisibility(gamers_character[i + from], gamers_character[i + from])

        gameobject.push(
          {
            'name': gamers_name[i + from],
            'email': gamers_email[i + from],
            'socketid': gamers_socketid[i + from],
            'current_quest': current_quest,
            'quest': quest,
            'gamestatus': gamestatus,
            'character': gamers_character[i + from],
            'goodvsevil': goodVsEvil,
            'aproval': aprovals_temp[i + from],
            'type': visi.type,
            'leader': false,
            'lady_lake': false,
            'selectByLeader': false,
            'sheild': false,
            'gestedMerlin': guestedMerlin[i + from],
            'guestperson': guestperson[i + from],
            'req': requered_selectors_in_game,
            'wonby': wonby,
            'rejected_count': rejected_count,
            //'accept_arry': aprovals[(i + from) - gamers_email.length],
            //'callaccept': callaccept,
            //'callSuccessFail': callSuccessFail,
            //'successfails': successfails[(i + from) - gamers_email.length],
            'onlinestatus': onlinestatus[(i + from)],
            'good_characters': good,//*
            'evil_caracters': evil,//*
            'calllink': callLink,
            'lady_select': "unkonwn",
            'questcombination': questcombination,
            'endgame': gameEnd
          }
        );
      }
    }
    console.log("game data", gameobject)
    return gameobject;

  }


  function restartquest() { //For restart quest when reject quest select
    startagainquest = true;
    rejected_count = rejected_count + 1;
    if (rejected_count == 5) {
      endgame('evil');
    } else {
      getgamedata();
      aprovals = aprovals.map(x => 'unknown');//for clear aprovals
      // aprovals_temp = aprovals_temp.map(x => 'unknown');//for clear aprovals
      selectLeader = selectLeader.map(x => false);//for clear selectors

      setTimeout(function () {
        aprovals_temp = aprovals_temp.map(x => 'unknown');//for clear aprovals
        lady_lake = lady_lake.map(x => "unknown");
        lady_request_count = 0;
        callaccept = false;
        callSuccessFail = false;
        successfails = successfails.map(x => "unknown");
        setLeader();
        getgamedata();
      }, 3000)

    }
  }


  function startNewGame() { //Restart new game
    questcombination = [{ total: '', fail_count: '' }, { total: '', fail_count: '' }, { total: '', fail_count: '' }, { total: '', fail_count: '' }, { total: '', fail_count: '' }];
    gameEnd = false;
    setQuest(current_quest);
    gamestatus = false;
    leader = leader.map(x => false);
    selectLeader = selectLeader.map(x => false);
    guestedMerlin = guestedMerlin.map(x => "unknown");
    gamers_character = gamers_character.map(x => "unknown");
    guestperson = guestperson.map(x => false);
    aprovals = aprovals.map(x => "unknown");
    aprovals_temp = aprovals_temp.map(x => "unknown");
    quest = ["unknown", "unknown", "unknown", "unknown", "unknown"];

    currentleader = -1;
    currentLady = -1;
    submited_count = 0;

    lady_lake = lady_lake.map(x => "unknown");
    lady_selector = lady_selector.map(x => "unknown");
    lady_request_count = 0;
    callaccept = false;
    callSuccessFail = false;
    successfails = successfails.map(x => "unknown");
    rejected_count = 0;
    privius_lady = [];

    getallcharacters();
    getgamedata();
  }


  function requestLakelady(data) { //Submit lady of the lake action

    console.log("selected by lady");
    let lady = gamers_email.indexOf(data.myemail);
    let check = gamers_email.indexOf(data.selectedemail);

    var posibility = count(privius_lady, data.selectedemail);//check wether selected person from lady is privius lady or not



    if (lady != check) {
      var v = visibility.getvisibility(gamers_character[check], gamers_character[check]);

      let type = v.type;

      if (lady_request_count == 0) {

        if (posibility != 0) {
          var err = { error: "Lady of the lake cannot see previous lady of the lakes", error_code: 0007 }
          io.in(room_id).emit('Error', err);
        } else {
          //lady_selector[gamers_email.indexOf(data.selectedemail)]=true;
          data = { type: type }
          lady_selector[check] = v.type;
          console.log("lady of lake selected", lady_selector);
          // io.to(gamers_socketid[lady]).emit('ResponseToLady', data);
          lady_request_count++;
          getgamedata()
        }
      }

    }
  }



  function count(a, i) {
    var result = 0;
    for (var o in a)
      if (a[o] == i)
        result++;
    return result;
  }

  function getallcharacters() {//For getting characters counts
    good = [];
    evil = [];

    for (var i = 0; i < gamers_character.length; i++) {
      var vis = visibility.getvisibility(gamers_character[i], gamers_character[i]);

      if (vis.type == "evil") {
        evil.push(gamers_character[i]);
      } else {
        good.push(gamers_character[i]);
      }
    }


  }


  function setCallLink(text) {
    callLink = text;
    getgamedata();
  }

  return {

    addgammer: addgammer,
    getgamedata: getgamedata,
    startgame: startgame,
    selectByLeader: selectByLeader,
    requiretoaproveal: requiretoaproveal,
    submitaproval: submitaproval,
    submitRequestOfSuccessOrFails: submitRequestOfSuccessOrFails,
    clickingGuestMerlin: clickingGuestMerlin,
    submitGuestedMerlin: submitGuestedMerlin,
    setQuest: setQuest,
    submitQuest: submitQuest,
    startNewGame: startNewGame,
    requestLakelady: requestLakelady,
    ladyLakeselecting: ladyLakeselecting,
    logout: logout,
    checkclientsstatus: checkclientsstatus,
    getcoutOfGammers: getcoutOfGammers,
    setCallLink: setCallLink
  };
}



server.listen(port, () => console.log(`Listening on port ${port}`));
