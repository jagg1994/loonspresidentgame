module.exports = {
    getvisibility: function (who, person) {

        var visibility = { "type": "unknown", "character": "unknown" };

        //const type = { "merlin": "good", "percival": "good", "servent1": "good", "servent2": "good", "servent3": "good", "servent4": "good", "servent5": "good", "servent6": "good", "assassin": "evil", "morgana": "evil", "mordred": "evil", "oberon": "evil", "minion_of_mordred1": "evil", "minion_of_mordred2": "evil", "minion_of_mordred3": "evil", "minion_of_mordred4": "evil" };
        var type = [];
        type["merlin"] = "good"; type["percival"] = "good"; type["servent1"] = "good"; type["servent2"] = "good"; type["servent3"] = "good"; type["servent4"] = "good"; type["servent5"] = "good"; type["servent6"] = "good";
        type["assassin"] = "evil"; type["morgana"] = "evil"; type["mordred"] = "evil"; type["oberon"] = "evil"; type["minion_of_mordred1"] = "evil"; type["minion_of_mordred2"] = "evil"; type["minion_of_mordred3"] = "evil"; type["minion_of_mordred4"] = "evil";

        if (who == person) {
            visibility = { "type": type[who], "character": who };
        } else{

            //********************************************************************************************************* */

            if (who == "merlin") {
                switch (person) {
                    case "morgana":
                        visibility = { "type": "evil", "character": "unknown" };
                        break;
                    case "assassin":
                        visibility = { "type": "evil", "character": "unknown" };
                        break;
                    case "oberon":
                        visibility = { "type": "evil", "character": "unknown" };
                        break;
                    case "minion_of_mordred1":
                        visibility = { "type": "evil", "character": "unknown" };
                        break;
                    case "minion_of_mordred2":
                        visibility = { "type": "evil", "character": "unknown" };
                        break;
                    case "minion_of_mordred3":
                        visibility = { "type": "evil", "character": "unknown" };
                        break;
                    case "minion_of_mordred4":
                        visibility = { "type": "evil", "character": "unknown" };
                        break;

                    default:
                        visibility = { "type": "unknown", "character": "unknown" };
                }
            }

        //******************************************************************************************************** */        
        if (who == "percival") {
            switch (person) {
                case "morgana":
                    visibility = { "type": "unknown", "character": "half" };
                    break;
                case "merlin":
                    visibility = { "type": "unknown", "character": "half" };
                    break;
                default:
                    visibility = { "type": "unknown", "character": "unknown" };
            }
        }

        //******************************************************************************************************** */        
        if ((who == "assassin" || who == "morgana" || who == "mordred" || who=="minion_of_mordred1" || who=="minion_of_mordred2"|| who=="minion_of_mordred3"|| who=="minion_of_mordred4") && (person == "assassin" || person == "morgana" || person == "mordred" || person=="minion_of_mordred1" || person=="minion_of_mordred2"|| person=="minion_of_mordred3"|| person=="minion_of_mordred4" )) {

            visibility = { "type": "evil", "character": "unknown" };

        }

    }

        return visibility;
    }
}