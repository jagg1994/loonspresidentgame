module.exports = {
    getcharacters: function (numberOfmembers, gamedata) {

        var morgana_percival = gamedata.morgana_percival;
        var mordred = gamedata.mordred;
        var oberon = gamedata.oberon;
        var allcharacters = [];
        var goodVsEvil = {};

        //Team 1, Without Percival & Morgana, Without Mordred, Without Oberon,
        if (!morgana_percival && !mordred && !oberon) {
            switch (numberOfmembers) {
                case 2: allcharacters = ["merlin", "assassin"];
                    goodVsEvil = { "good": 1, "evil": 1 };
                    break;
                case 5:
                    allcharacters = ["merlin", "servent1", "servent2", "assassin", "minion_of_mordred1"];
                    goodVsEvil = { "good": 3, "evil": 2 };
                    break;
                case 6:
                    allcharacters = ["merlin", "servent1", "servent2", "servent3", "assassin", "minion_of_mordred1"];
                    goodVsEvil = { "good": 4, "evil": 2 };
                    break;
                case 7:
                    allcharacters = ["merlin", "servent1", "servent2", "servent3", "assassin", "minion_of_mordred1", "minion_of_mordred2"];
                    goodVsEvil = { "good": 4, "evil": 3 };
                    break;
                case 8:
                    allcharacters = ["merlin", "servent1", "servent2", "servent3", "servent4", "assassin", "minion_of_mordred1", "minion_of_mordred2"];
                    goodVsEvil = { "good": 5, "evil": 3 };
                    break;
                case 9:
                    allcharacters = ["merlin", "servent1", "servent2", "servent3", "servent4", "assassin", "minion_of_mordred1", "minion_of_mordred2", "minion_of_mordred3"];
                    goodVsEvil = { "good": 5, "evil": 4 };
                    break;
                case 10:
                    allcharacters = ["merlin", "servent1", "servent2", "servent3", "servent4", "servent5", "assassin", "minion_of_mordred1", "minion_of_mordred2", "minion_of_mordred3"];
                    goodVsEvil = { "good": 6, "evil": 4 };
                    break;
                case 11:
                    allcharacters = ["merlin", "servent1", "servent2", "servent3", "servent4", "servent5", "servent6", "assassin", "minion_of_mordred1", "minion_of_mordred2", "minion_of_mordred3"];
                    goodVsEvil = { "good": 7, "evil": 4 };
                    break;
                case 12:
                    allcharacters = ["merlin", "servent1", "servent2", "servent3", "servent4", "servent5", "servent6", "assassin", "minion_of_mordred1", "minion_of_mordred2", "minion_of_mordred3", "minion_of_mordred4"];
                    goodVsEvil = { "good": 7, "evil": 5 };
                    break;
                // code block
            }
        }
        //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++=

        //Team 2, With Percival & Morgana, Without Mordred, Without Oberon,
        if (morgana_percival && !mordred && !oberon) {
            switch (numberOfmembers) {
                case 5:
                    allcharacters = ["merlin", "percival", "servent1", "assassin", "morgana"];
                    goodVsEvil = { "good": 3, "evil": 2 };
                    break;
                case 6:
                    allcharacters = ["merlin", "percival", "servent1", "servent2", "assassin", "morgana"];
                    goodVsEvil = { "good": 4, "evil": 2 };
                    break;
                case 7:
                    allcharacters = ["merlin", "percival", "servent1", "servent2", "assassin", "morgana", "minion_of_mordred1"];
                    goodVsEvil = { "good": 4, "evil": 3 };
                    break;
                case 8:
                    allcharacters = ["merlin", "percival", "servent1", "servent2", "servent3", "assassin", "morgana", "minion_of_mordred1"];
                    goodVsEvil = { "good": 5, "evil": 3 };
                    break;
                case 9:
                    allcharacters = ["merlin", "percival", "servent1", "servent2", "servent3", "assassin", "morgana", "minion_of_mordred1", "minion_of_mordred2"];
                    goodVsEvil = { "good": 5, "evil": 4 };
                    break;
                case 10:
                    allcharacters = ["merlin", "percival", "servent1", "servent2", "servent3", "servent4", "assassin", "morgana", "minion_of_mordred1", "minion_of_mordred2"];
                    goodVsEvil = { "good": 6, "evil": 4 };
                    break;
                case 11:
                    allcharacters = ["merlin", "percival", "servent1", "servent2", "servent3", "servent4", "servent5", "assassin", "morgana", "minion_of_mordred1", "minion_of_mordred2"];
                    goodVsEvil = { "good": 6, "evil": 4 };
                    break;
                case 12:
                    allcharacters = ["merlin", "percival", "servent1", "servent2", "servent3", "servent4", "servent5", "assassin", "morgana", "minion_of_mordred1", "minion_of_mordred2", "minion_of_mordred3"];
                    goodVsEvil = { "good": 6, "evil": 5 };
                    break;


            }
        }
        //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++=

        //Team 3, With Percival & Morgana, With Mordred, Without Oberon,
        if (morgana_percival && mordred && !oberon) {
            switch (numberOfmembers) {
                case 5:
                    allcharacters = ["merlin", "percival", "servent1", "morgana", "mordred"];
                    goodVsEvil = { "good": 3, "evil": 2 };
                    break;
                case 6:
                    allcharacters = ["merlin", "percival", "servent1", "servent2", "morgana", "mordred"];
                    goodVsEvil = { "good": 4, "evil": 2 };
                    break;
                case 7:
                    allcharacters = ["merlin", "percival", "servent1", "servent2", "morgana", "mordred", "minion_of_mordred1"];
                    goodVsEvil = { "good": 4, "evil": 3 };
                    break;
                case 8:
                    allcharacters = ["merlin", "percival", "servent1", "servent2", "servent3", "morgana", "mordred", "minion_of_mordred1"];
                    goodVsEvil = { "good": 5, "evil": 3 };
                    break;
                case 9:
                    allcharacters = ["merlin", "percival", "servent1", "servent2", "servent3", "morgana", "mordred", "minion_of_mordred1", "minion_of_mordred2"];
                    goodVsEvil = { "good": 5, "evil": 4 };
                    break;
                case 10:
                    allcharacters = ["merlin", "percival", "servent1", "servent2", "servent3", "servent4", "morgana", "mordred", "minion_of_mordred1", "minion_of_mordred2"];
                    goodVsEvil = { "good": 6, "evil": 4 };
                    break;
                case 11:
                    allcharacters = ["merlin", "percival", "servent1", "servent2", "servent3", "servent4", "servent5", "morgana", "mordred", "minion_of_mordred1", "minion_of_mordred2"];
                    goodVsEvil = { "good": 7, "evil": 4 };
                    break;
                case 12:
                    allcharacters = ["merlin", "percival", "servent1", "servent2", "servent3", "servent4", "servent5", "morgana", "mordred", "minion_of_mordred1", "minion_of_mordred2", "minion_of_mordred3"];
                    goodVsEvil = { "good": 7, "evil": 5 };
                    break;


            }
        }

        //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++=

        //Team 4, With Percival & Morgana, With Mordred, With Oberon,
        if (morgana_percival && mordred && oberon) {
            switch (numberOfmembers) {
                case 5:
                    allcharacters = ["merlin", "percival", "servent1", "mordred", "morgana"];
                    goodVsEvil = { "good": 3, "evil": 2 };
                    break;
                case 6:
                    allcharacters = ["merlin", "percival", "servent1", "servent2", "mordred", "morgana"];
                    goodVsEvil = { "good": 4, "evil": 2 };
                    break;
                case 7:
                    allcharacters = ["merlin", "percival", "servent1", "servent2", "mordred", "morgana", "oberon"];
                    goodVsEvil = { "good": 4, "evil": 3 };
                    break;
                case 8:
                    allcharacters = ["merlin", "percival", "servent1", "servent2", "servent3", "mordred", "morgana", "oberon"];
                    goodVsEvil = { "good": 5, "evil": 3 };
                    break;
                case 9:
                    allcharacters = ["merlin", "percival", "servent1", "servent2", "servent3", "mordred", "morgana", "oberon", "minion_of_mordred1"];
                    goodVsEvil = { "good": 5, "evil": 4 };
                    break;
                case 10:
                    allcharacters = ["merlin", "percival", "servent1", "servent2", "servent3", "servent4", "mordred", "morgana", "oberon", "minion_of_mordred1"];
                    goodVsEvil = { "good": 6, "evil": 4 };
                    break;
                case 11:
                    allcharacters = ["merlin", "percival", "servent1", "servent2", "servent3", "servent4", "servent5", "mordred", "morgana", "oberon", "minion_of_mordred1"];
                    goodVsEvil = { "good": 7, "evil": 4 };
                    break;
                case 12:
                    allcharacters = ["merlin", "percival", "servent1", "servent2", "servent3", "servent4", "servent5", "mordred", "morgana", "oberon", "minion_of_mordred1", "minion_of_mordred2"];
                    goodVsEvil = { "good": 7, "evil": 5 };
                    break;


            }
        }

        //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++=

        //Team 5, Without Percival & Morgana, Without Mordred, With Oberon,
        if (!morgana_percival && !mordred && oberon) {
            switch (numberOfmembers) {
                case 5:
                    allcharacters = ["merlin", "servent1", "servent2", "assassin", "oberon"];
                    goodVsEvil = { "good": 3, "evil": 2 };
                    break;
                case 6:
                    allcharacters = ["merlin", "servent1", "servent2", "servent3", "assassin", "oberon"];
                    goodVsEvil = { "good": 4, "evil": 2 };
                    break;
                case 7:
                    allcharacters = ["merlin", "servent1", "servent2", "servent3", "assassin", "oberon", "minion_of_mordred1"];
                    goodVsEvil = { "good": 4, "evil": 3 };
                    break;
                case 8:
                    allcharacters = ["merlin", "servent1", "servent2", "servent3", "servent4", "assassin", "oberon", "minion_of_mordred1"];
                    goodVsEvil = { "good": 5, "evil": 3 };
                    break;
                case 9:
                    allcharacters = ["merlin", "servent1", "servent2", "servent3", "servent4", "assassin", "oberon", "minion_of_mordred1", "minion_of_mordred2"];
                    goodVsEvil = { "good": 5, "evil": 4 };
                    break;
                case 10:
                    allcharacters = ["merlin", "servent1", "servent2", "servent3", "servent4", "servent5", "assassin", "oberon", "minion_of_mordred1", "minion_of_mordred2"];
                    goodVsEvil = { "good": 6, "evil": 4 };
                    break;
                case 11:
                    allcharacters = ["merlin", "servent1", "servent2", "servent3", "servent4", "servent5", "servent6", "assassin", "oberon", "minion_of_mordred1", "minion_of_mordred2"];
                    goodVsEvil = { "good": 7, "evil": 4 };
                    break;
                case 12:
                    allcharacters = ["merlin", "servent1", "servent2", "servent3", "servent4", "servent5", "servent6", "assassin", "oberon", "minion_of_mordred1", "minion_of_mordred2", "minion_of_mordred3"];
                    goodVsEvil = { "good": 7, "evil": 5 };
                    break;


            }
        }

        //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++=

        //Team 6, Without Percival & Morgana, With Mordred, Without Oberon,
        if (!morgana_percival && mordred && !oberon) {
            switch (numberOfmembers) {
                case 5:
                    allcharacters = ["merlin", "servent1", "servent2", "assassin", "mordred"];
                    goodVsEvil = { "good": 3, "evil": 2 };
                    break;
                case 6:
                    allcharacters = ["merlin", "servent1", "servent2", "servent3", "assassin", "mordred"];
                    goodVsEvil = { "good": 4, "evil": 2 };
                    break;
                case 7:
                    allcharacters = ["merlin", "servent1", "servent2", "servent3", "assassin", "mordred", "minion_of_mordred1"];
                    goodVsEvil = { "good": 4, "evil": 3 };
                    break;
                case 8:
                    allcharacters = ["merlin", "servent1", "servent2", "servent3", "servent4", "assassin", "mordred", "minion_of_mordred1"];
                    goodVsEvil = { "good": 5, "evil": 3 };
                    break;
                case 9:
                    allcharacters = ["merlin", "servent1", "servent2", "servent3", "servent4", "assassin", "mordred", "minion_of_mordred1", "minion_of_mordred2"];
                    goodVsEvil = { "good": 5, "evil": 4 };
                    break;
                case 10:
                    allcharacters = ["merlin", "servent1", "servent2", "servent3", "servent4", "servent5", "assassin", "mordred", "minion_of_mordred1", "minion_of_mordred2"];
                    goodVsEvil = { "good": 6, "evil": 4 };
                    break;
                case 11:
                    allcharacters = ["merlin", "servent1", "servent2", "servent3", "servent4", "servent5", "servent6", "assassin", "mordred", "minion_of_mordred1", "minion_of_mordred2"];
                    goodVsEvil = { "good": 7, "evil": 4 };
                    break;
                case 12:
                    allcharacters = ["merlin", "servent1", "servent2", "servent3", "servent4", "servent5", "servent6", "assassin", "mordred", "minion_of_mordred1", "minion_of_mordred2", "minion_of_mordred3"];
                    goodVsEvil = { "good": 7, "evil": 5 };
                    break;


            }
        }
        //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++=

        //Team 7, Without Percival & Morgana, With Mordred, With Oberon,
        if (!morgana_percival && mordred && oberon) {
            switch (numberOfmembers) {
                case 5:
                    allcharacters = ["merlin", "servent1", "servent2", "mordred", "oberon"];
                    goodVsEvil = { "good": 3, "evil": 2 };
                    break;
                case 6:
                    allcharacters = ["merlin", "servent1", "servent2", "servent3", "mordred", "oberon"];
                    goodVsEvil = { "good": 4, "evil": 2 };
                    break;
                case 7:
                    allcharacters = ["merlin", "servent1", "servent2", "servent3", "mordred", "oberon", "assassin"];
                    goodVsEvil = { "good": 4, "evil": 3 };
                    break;
                case 8:
                    allcharacters = ["merlin", "servent1", "servent2", "servent3", "servent4", "mordred", "oberon", "assassin"];
                    goodVsEvil = { "good": 5, "evil": 3 };
                    break;
                case 9:
                    allcharacters = ["merlin", "servent1", "servent2", "servent3", "servent4", "mordred", "oberon", "assassin", "minion_of_mordred1"];
                    goodVsEvil = { "good": 5, "evil": 4 };
                    break;
                case 10:
                    allcharacters = ["merlin", "servent1", "servent2", "servent3", "servent4", "servent5", "mordred", "oberon", "assassin", "minion_of_mordred1"];
                    goodVsEvil = { "good": 6, "evil": 4 };
                    break;
                case 11:
                    allcharacters = ["merlin", "servent1", "servent2", "servent3", "servent4", "servent5", "servent6", "mordred", "oberon", "assassin", "minion_of_mordred1"];
                    goodVsEvil = { "good": 7, "evil": 4 };
                    break;
                case 12:
                    allcharacters = ["merlin", "servent1", "servent2", "servent3", "servent4", "servent5", "servent6", "mordred", "oberon", "assassin", "minion_of_mordred1", "minion_of_mordred2"];
                    goodVsEvil = { "good": 7, "evil": 5 };
                    break;


            }
        }

        //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++=

        //Team 8, With Percival & Morgana, Without Mordred, With Oberon,
        if (morgana_percival && !mordred && oberon) {
            switch (numberOfmembers) {
                case 5:
                    allcharacters = ["merlin", "percival", "servent1", "morgana", "oberon"];
                    goodVsEvil = { "good": 3, "evil": 2 };
                    break;
                case 6:
                    allcharacters = ["merlin", "percival", "servent1", "servent2", "morgana", "oberon"];
                    goodVsEvil = { "good": 4, "evil": 2 };
                    break;
                case 7:
                    allcharacters = ["merlin", "percival", "servent1", "servent2", "morgana", "oberon", "minion_of_mordred1"];
                    goodVsEvil = { "good": 4, "evil": 3 };
                    break;
                case 8:
                    allcharacters = ["merlin", "percival", "servent1", "servent2", "servent3", "morgana", "oberon", "minion_of_mordred1"];
                    goodVsEvil = { "good": 5, "evil": 3 };
                    break;
                case 9:
                    allcharacters = ["merlin", "percival", "servent1", "servent2", "servent3", "morgana", "oberon", "minion_of_mordred1", "minion_of_mordred2"];
                    goodVsEvil = { "good": 5, "evil": 4 };
                    break;
                case 10:
                    allcharacters = ["merlin", "percival", "servent1", "servent2", "servent3", "servent4", "morgana", "oberon", "minion_of_mordred1", "minion_of_mordred2"];
                    goodVsEvil = { "good": 6, "evil": 4 };
                    break;
                case 11:
                    allcharacters = ["merlin", "percival", "servent1", "servent2", "servent3", "servent4", "servent5", "morgana", "oberon", "minion_of_mordred1", "minion_of_mordred2"];
                    goodVsEvil = { "good": 7, "evil": 4 };
                    break;
                case 12:
                    allcharacters = ["merlin", "percival", "servent1", "servent2", "servent3", "servent4", "servent5", "morgana", "oberon", "minion_of_mordred1", "minion_of_mordred2", "minion_of_mordred3"];
                    goodVsEvil = { "good": 7, "evil": 5 };
                    break;


            }
        }



        return { "allcharacters": allcharacters, "goodVsEvil": goodVsEvil };
    },

};