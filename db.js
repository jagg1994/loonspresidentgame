var mongoose = require('mongoose');


mongoose.connect('mongodb://localhost:27017/avlonedb', {useNewUrlParser: true});

var db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', function() {
  console.log("connected to db")
});


/*
mongoose.connect('mongodb://localhost:27017/avlonedb').then(() => {
console.log("Connected to Database");
}).catch((err) => {
console.log("Not Connected to Database ERROR! ", err);
});
*/