module.exports = {
    getselectorcount: function (numberOfmembers) {
        var data = [];

        switch (numberOfmembers) {
            case 2:
                data = [{ count: 2, evil_count: 1 }, { count: 3, evil_count: 1 }, { count: 2, evil_count: 1 }, { count: 3, evil_count: 1 }, { count: 3, evil_count: 1 }];
                break;
            case 5:
                data = [{ count: 2, evil_count: 1 }, { count: 3, evil_count: 1 }, { count: 2, evil_count: 1 }, { count: 3, evil_count: 1 }, { count: 3, evil_count: 1 }];
                
                break;
            case 6:
                data = [{ count: 2, evil_count: 1 }, { count: 3, evil_count: 1 }, { count: 4, evil_count: 1 }, { count: 3, evil_count: 1 }, { count: 4, evil_count: 1 }];


                break;
            case 7:
                data = [{ count: 2, evil_count: 1 }, { count: 3, evil_count: 1 }, { count: 3, evil_count: 1 }, { count: 4, evil_count: 2 }, { count: 4, evil_count: 1 }];


                break;
            case 8:
                data = [{ count: 3, evil_count: 1 }, { count: 4, evil_count: 1 }, { count: 4, evil_count: 1 }, { count: 5, evil_count: 2 }, { count: 5, evil_count: 1 }];

                break;
            case 9:
                data = [{ count: 3, evil_count: 1 }, { count: 4, evil_count: 1 }, { count: 4, evil_count: 1 }, { count: 5, evil_count: 2 }, { count: 5, evil_count: 1 }];

                break;
            case 10:
                data = [{ count: 3, evil_count: 1 }, { count: 4, evil_count: 1 }, { count: 4, evil_count: 1 }, { count: 5, evil_count: 2 }, { count: 5, evil_count: 1 }];

                break;
            case 11:
                data = [{ count: 4, evil_count: 1 }, { count: 4, evil_count: 1 }, { count: 5, evil_count: 1 }, { count: 5, evil_count: 2 }, { count: 6, evil_count: 1 }];

                break;
            case 12:
                data = [{ count: 4, evil_count: 1 }, { count: 4, evil_count: 1 }, { count: 5, evil_count: 1 }, { count: 5, evil_count: 2 }, { count: 6, evil_count: 1 }];

                break;
            default:
                data = [{ count: 2, evil_count: 1 }, { count: 3, evil_count: 1 }, { count: 2, evil_count: 1 }, { count: 3, evil_count: 1 }, { count: 3, evil_count: 1 }];
                
        }

        return data;
    }
}