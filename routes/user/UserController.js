var express = require('express');
var router = express();
var nodeMailer = require('nodemailer');
var bodyParser = require('body-parser');
var md5 = require('md5');
var mail = require('./../email/email');

router.use(function (req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});
router.use(bodyParser.json());
router.use(express.static('public'));
router.use(bodyParser.urlencoded({
    extended: true
}));
var User = require('./user');

// CREATES A NEW USER
router.post('/', function (req, res) {

    User.findOne({ email: req.body.email }, function (err, user) {
        if (err) {
            console.log(err);
        }
        var message;
        if (user) {
            console.log(user)
            message = "User alrady registered with this email";

            res.json({ message: message, success: false });
        } else {

            var encr_pawd = md5(req.body.password);
            console.log("MD5 hash (as a hex string) = " + encr_pawd);


            User.create({
                name: req.body.name,
                email: req.body.email,
                password: encr_pawd,
                verify:false
            },
                function (err, user) {
                    if (err) return res.status(500).send("There was a problem adding the information to the database.");
                    //res.status(200).send(user);

                    if (!err) {
                        var userdata = {
                            id: user._id,
                            name: user.name,
                            email: user.email,
                            verify:user.verify
                        }
                        var data = {
                            data: userdata,
                            success: true,
                        };
                        // responds with status code 200 and data
                        res.status(200).json(data);
                        mail.sendingmail(user._id);
                    }

                });
        }

    });

});




router.post('/login', function (req, res) {

    User.findOne(
        {
            $and: [
                { email: req.body.email },
                { password: md5(req.body.password) }
            ]
        }
        , function (err, user) {

            if (err) return res.status(500).send("There was a problem adding the information to the database.");
            //res.status(200).send(user);

            if (!err) {

                var message;
                if (user) {

                    if(user.verify){
                    var userdata = {
                        id: user._id,
                        name: user.name,
                        email: user.email,
                    }
                    var data = {
                        data: userdata,
                        success: true,
                    };
                    // responds with status code 200 and data
                    res.status(200).json(data);
                }else{
                    message = "Pleace verify your email address";
                    res.json({ message: message, success: false });
                }

                } else {
                    message = "Invalid user Credentials";
                    res.json({ message: message, success: false });
                }
            }
        });

});

// RETURNS ALL THE USERS IN THE DATABASE
router.get('/all', function (req, res) {
    User.find({}, function (err, users) {
        if (err) return res.status(500).send("There was a problem finding the users.");
        res.status(200).send(users);
    });
});
router.get('/email', function (req, res) {
   // mail.sendingmail();
   User.findByIdAndUpdate(
    {_id: '5eca9621205c4b2120d1cf6c'}, // query
    {$set: {verify: false}}, // replacement, replaces only the field "hi"
    {}, // options
    function(err, object) {
        if (err){
            console.warn(err.message);  // returns error if no matching object found
        }else{
            res.status(200).send(object);
            console.dir(object);
        }
    });

});
// GETS A SINGLE USER FROM THE DATABASE
router.get('/:id', function (req, res) {
    User.findById(req.params.id, function (err, user) {
        if (err) return res.status(500).send("There was a problem finding the user.");
        if (!user) return res.status(404).send("No user found.");
        res.status(200).send(user);
    });
});

// DELETES A USER FROM THE DATABASE
router.delete('/:id', function (req, res) {
    User.findByIdAndRemove(req.params.id, function (err, user) {
        if (err) return res.status(500).send("There was a problem deleting the user.");
        res.status(200).send("User: " + user.name + " was deleted.");
    });
});
router.delete('/', function (req, res) {
    User.remove(function (err, user) {
        if (err) return res.status(500).send("There was a problem deleting the user.");
        res.status(200).send("User: " + user.name + " was deleted.");
    });
});
// UPDATES A SINGLE USER IN THE DATABASE
router.put('/:id', function (req, res) {
    User.findByIdAndUpdate(req.params.id, req.body, { new: true }, function (err, user) {
        if (err) return res.status(500).send("There was a problem updating the user.");
        res.status(200).send(user);
    });
});





module.exports = router;