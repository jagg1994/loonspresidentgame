var nodeMailer = require('nodemailer');
var User = require('./../user/user');
var hbs = require('nodemailer-express-handlebars');

module.exports = {
    sendingmail: function (userid) {


        User.findById(userid, function (err, user) {
            if (err) return res.status(500).send("There was a problem finding the user.");
            if (!user) return res.status(404).send("No user found.");

            var name = user.name;
            var id = user._id;
            var email = user.email;
            // res.status(200).send(user);
            //var url = "http://localhost:4001/verification/?user=" + id + "&option=avalon.online";
            var url = "http://avalonteam.online/verification/?user=" + id + "&option=avalon.online";

            send(name, url, email);

        });
    }

}

function send(name, url, email) {


    let transporter = nodeMailer.createTransport({
        host: 'smtp.gmail.com',
        port: 465,
        secure: true,
        auth: {
            // should be replaced with real sender's account
            user: 'loonslabgame@gmail.com',
            pass: 'Avalon@loonslabmail55'
        }
    });

    const handlebarOptions = {
        viewEngine: {
            extName: '.handlebars',
            partialsDir: './routes/email/views',
            layoutsDir: './routes/email/views',
            defaultLayout: 'verifyEmail.handlebars',
        },
        viewPath: './routes/email/views',
        extName: '.handlebars',
    };

    transporter.use('compile', hbs(handlebarOptions));

    var mailOptions = {
        from: 'Avalonteam',
        to: email,
        subject: 'Avalonteam verification',
        // html: '<h1>Welcome ' + name + '</h1><p>That was easy!</p> <a> ' + url + ' </a>',
        template: 'verifyEmail',
        context: {

            name: name,
            verifyURL: url
        }
    };

    transporter.sendMail(mailOptions, function (error, info) {
        if (error) {
            console.log(error);
        } else {
            console.log('Email sent: ' + info.response);
        }
    });




}