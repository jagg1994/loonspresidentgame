
var User = require('./../user/user');
const path = require("path");

module.exports = {
    verify: function (userid) {

        User.findByIdAndUpdate(
            {_id: userid}, // query
            {$set: {verify: true}}, // replacement, replaces only the field "hi"
            {}, // options
            function(err, object) {
                if (err){
                    console.warn(err.message);  // returns error if no matching object found
                }else{
                    console.dir(object);
                }
            });
        
    }

}