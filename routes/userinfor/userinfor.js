var mongoose = require('mongoose');  
var UserinfoSchema = new mongoose.Schema({  
  country: String,
  regionName: String,
  city: String,
  zip: String,
  lat: String,
  lon: String,
  isp: String,
  ip: String,
});
mongoose.model('Userinfor', UserinfoSchema);

module.exports = mongoose.model('Userinfor');