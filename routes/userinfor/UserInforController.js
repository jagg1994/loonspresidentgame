var express = require('express');
var router = express();
var bodyParser = require('body-parser');
var md5 = require('md5');

router.use(function (req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});
router.use(bodyParser.json());
router.use(express.static('public'));
router.use(bodyParser.urlencoded({
    extended: true
}));
var User = require('./userinfor');

// CREATES A NEW USER
router.post('/', function (req, res) {

   
            User.create({
                country:req.body.country ,
                regionName:req.body.regionName ,
                city:req.body.city ,
                zip:req.body.zip ,
                lat:req.body.lat ,
                lon:req.body.lon ,
                isp:req.body.isp ,
                ip:req.body.query ,


            },
            function (err, user) {
                if (err) return res.status(500).send("There was a problem adding the information to the database.");
                //res.status(200).send(user);

                if (!err) {
                    var userdata = {
                        id: user._id,
                        
                    }
                    var data = {
                        data: userdata,
                        success: true,
                    };
                    // responds with status code 200 and data
                    res.status(200).json(data);
                }

            });
       

});





// RETURNS ALL THE USERS IN THE DATABASE
router.get('/', function (req, res) {
    User.find({}, function (err, users) {
        if (err) return res.status(500).send("There was a problem finding the users.");
        res.status(200).send(users);
    });
});

// GETS A SINGLE USER FROM THE DATABASE
router.get('/:id', function (req, res) {
    User.findById(req.params.id, function (err, user) {
        if (err) return res.status(500).send("There was a problem finding the user.");
        if (!user) return res.status(404).send("No user found.");
        res.status(200).send(user);
    });
});

// DELETES A USER FROM THE DATABASE
router.delete('/:id', function (req, res) {
    User.findByIdAndRemove(req.params.id, function (err, user) {
        if (err) return res.status(500).send("There was a problem deleting the user.");
        res.status(200).send("User: " + user.name + " was deleted.");
    });
});
router.delete('/', function (req, res) {
    User.remove(function (err, user) {
        if (err) return res.status(500).send("There was a problem deleting the user.");
        res.status(200).send("User: " + user.name + " was deleted.");
    });
});
// UPDATES A SINGLE USER IN THE DATABASE
router.put('/:id', function (req, res) {
    User.findByIdAndUpdate(req.params.id, req.body, { new: true }, function (err, user) {
        if (err) return res.status(500).send("There was a problem updating the user.");
        res.status(200).send(user);
    });
});


module.exports = router;