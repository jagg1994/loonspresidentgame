
import React from "react";

import './Login.css';
// reactstrap components
import {
  Button,
  Card,
  CardHeader,
  CardBody,
  FormGroup,
  Form,
  Input,
  InputGroupAddon,
  InputGroupText,
  InputGroup,
  Container,
  Row,
  Col,
  Alert
} from "reactstrap";
import { Redirect, useParams } from "react-router-dom";
import cookie from 'react-cookies';
import * as Appconst from '../../Appcosnt';
// core components
import DemoNavbar from "./../../components/Navbars/DemoNavbar.js";
import SimpleFooter from "./../../components/Footers/SimpleFooter.js";
import FacebookLogin from 'react-facebook-login/dist/facebook-login-render-props'
import GoogleLogin from 'react-google-login';

import background_image from "../../assets/img/loginscreen_background.jpg";


class Login extends React.Component {

  constructor(props) {
    super(props)

    this.state = {
      room: '',
      fields: {},
      errors: {},

      errorAlert: 'none',
      errormassage: ''
    }
  }

  componentDidMount() {

    document.documentElement.scrollTop = 0;
    document.scrollingElement.scrollTop = 0;
    this.refs.main.scrollTop = 0;
    let paramId = this.props.match.params.roomId;

    this.setState({ room: paramId })

  }

  handleValidation() {
    let fields = this.state.fields;
    let errors = {};
    let formIsValid = true;

    //Email
    if (!fields["email"]) {
      formIsValid = false;
      errors["email"] = "Cannot be empty";
    }

    if (typeof fields["email"] !== "undefined") {
      let lastAtPos = fields["email"].lastIndexOf('@');
      let lastDotPos = fields["email"].lastIndexOf('.');

      if (!(lastAtPos < lastDotPos && lastAtPos > 0 && fields["email"].indexOf('@@') == -1 && lastDotPos > 2 && (fields["email"].length - lastDotPos) > 2)) {
        formIsValid = false;
        errors["email"] = "Email is not valid";
      }
    }

    if (!fields["password"]) {
      formIsValid = false;
      errors["password"] = "Cannot be empty";
    }

    this.setState({ errors: errors });
    return formIsValid;
  }

  handleChange(field, e) {
    let fields = this.state.fields;
    fields[field] = e.target.value;
    this.setState({ fields });
  }

  submit() {
    if (this.handleValidation()) {
      this.login();
    } else {

    }
  }

  login() {
    let fields = this.state.fields;

    fetch(Appconst.LOGIN, {//otp sent route
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        "email": fields["email"],
        "password": fields["password"]

      }),
    })
      .then((response) => response.json())
      .then((responseJson) => {
        console.log('response', responseJson)
        if (responseJson.success == true) {
          cookie.save('name', responseJson.data.name);
          cookie.save('email', responseJson.data.email);

          let roomId = this.state.room;

          if (roomId != undefined) {
            this.props.history.push('/game/' + roomId);
          } else {
            this.props.history.push('/');
          }


        } else {
          this.setState({
            errorAlert: 'flex',
            errormassage: responseJson.message
          })

          setTimeout(function () { //Start the timer
            this.setState({
              errorAlert: 'none',
              errormassage: ''
            })
          }.bind(this), 2000)
        }
      })
      .catch((error) => {

      });

  }


  reg() {
    let roomId = this.state.room;

    if (roomId != undefined) {
      this.props.history.push('/Registration/' + roomId);
    } else {
      this.props.history.push('/Registration');
    }
  }



  responseFacebook(response) {
    console.log(response);
  }

  componentClicked() {
    console.log("ok");
  }



  render() {

    const responseGoogle = (response) => {
      console.log("response",response);
      //console.log("login name",response.profileObj.givenName);
if(response!=null){
      // cookie.save('name', response.profileObj.givenName);
      // cookie.save('email', response.profileObj.email);

      // let roomId = this.state.room;

      // if (roomId != undefined) {
      //   this.props.history.push('/game/' + roomId);
      // } else {
      //   this.props.history.push('/');
      // }
    }


    }


    const responseFacebook = (response) => {
      console.log("fb login",response);
    }
    const componentClicked = (response) => {
      console.log(response);
    }

    return (
      <>

        <main class="login_main" ref="main" style={{ backgroundImage: `url(${background_image})`, backgroundSize: 'cover' }}>
          <Alert style={{ position: 'fixed', width: '100%', zIndex: 2, display: this.state.errorAlert }} color="danger">
            {this.state.errormassage}
          </Alert>
          <section className="section section-shaped " style={{ backgroundColor: 'rgba(0, 0, 0, 0.53)', height: "100%" }}>

            <Container >
              <Row className="justify-content-center">
                <Col lg="5">
                  <Card id="login_form" className="bg-secondary shadow border-0">
                    <CardHeader className="pb-0" style={{ backgroundColor: 'transparent' }}>
                      <div className="text-muted text-center mb-3">
                        <small className="textOn_wood">Sign in with</small>
                      </div>
                      <div className="btn-wrapper text-center">




{/*
                        <FacebookLogin
                          appId="725108461363623"
                          autoLoad={true}
                          fields="name,email"
                          //onClick={componentClicked}
                          //callback={responseFacebook}
                          render={renderProps => (
                            <Button
                              className="btn-neutral disable btn-icon transparent_button"
                              color="default"
                              href="#pablo"
                              onClick={renderProps.onClick}
                            >
                              <span className="btn-inner--icon mr-1">
                                <img
                                  alt="..."
                                  src={require("./../../assets/img/icons/common/google.svg")}
                                />
                              </span>
                              <span className="btn-inner--text">Facebook</span>
                            </Button>
                          )}
                          />*/}


                        <GoogleLogin
                          clientId="475980110378-peu317bcv1mqjmt939ktg0f2on14d5ob.apps.googleusercontent.com"
                          buttonText="Login"
                          onSuccess={responseGoogle}
                          onFailure={responseGoogle}
                          cookiePolicy={'single_host_origin'}

                          render={renderProps => (
                            <Button
                            className="btn-neutral btn-icon ml-1 transparent_button"
                            color="default"
                            href="#pablo"
                            onClick={renderProps.onClick}
                            disabled={renderProps.disabled}
                          >
                            <span className="btn-inner--icon mr-1">
                              <img
                                alt="..."
                                src={require("./../../assets/img/icons/common/google.svg")}
                              />
                            </span>
                            <span className="btn-inner--text">Google</span>
                          </Button>
                            )}
                        />



                       
                      </div>
                    </CardHeader>
                    <CardBody className="px-lg-5 py-lg-5">
                      <div className="text-center text-muted mb-4">
                        <small className="textOn_wood">Or sign in with credentials</small>
                      </div>
                      <Form role="form">



                        <FormGroup className="mb-3">
                          <InputGroup className="input-group-alternative">
                            <InputGroupAddon addonType="prepend">
                              <InputGroupText className="transparent_button">
                                <i className="ni ni-email-83" />
                              </InputGroupText>
                            </InputGroupAddon>
                            <Input placeholder="Email" type="email" className="transparent_button"
                              onChange={e => this.handleChange("email", e)}
                              value={this.state.fields["email"]} />
                          </InputGroup>
                          <span className="validation_error">{this.state.errors["email"]}</span>
                        </FormGroup>
                        <FormGroup>
                          <InputGroup className="input-group-alternative">
                            <InputGroupAddon addonType="prepend">
                              <InputGroupText className="transparent_button">
                                <i className="ni ni-lock-circle-open" />
                              </InputGroupText>
                            </InputGroupAddon>
                            <Input
                              className="transparent_button"
                              placeholder="Password"
                              type="password"
                              autoComplete="off"
                              onChange={e => this.handleChange("password", e)}
                              value={this.state.fields["password"]}
                            />
                          </InputGroup>
                          <span className="validation_error">{this.state.errors["password"]}</span>
                        </FormGroup>

                        <div className="text-center">
                          <Button
                            className="my-3 transparent_button"
                            onClick={() => this.submit()}
                            type="button"
                          >
                            Sign in
                          </Button>
                        </div>
                      </Form>

                      <Row className="mt-3 row-without-margin">
                        <Col xs="6">
                          <a
                            className="text-light"
                            href="#pablo"
                            onClick={e => e.preventDefault()}
                          >
                            <small className="textOn_wood">Forgot password?</small>
                          </a>
                        </Col>
                        <Col className="text-right" xs="6">

                          <a
                            className="text-light"
                            // href="/registration"
                            // onClick={e => this.history.pushState(null, 'registration')}
                            onClick={e => this.reg()}
                          >
                            <small className="textOn_wood" style={{ cursor: 'pointer' }}>Create new account</small>
                          </a>
                        </Col>
                      </Row>

                    </CardBody>
                  </Card>

                </Col>
              </Row>
            </Container>
          </section>
        </main>

      </>
    );
  }
}

export default Login;
