
import React from "react";
import './Registration.css';
// reactstrap components
import {
  Button,
  Card,
  CardHeader,
  CardBody,
  FormGroup,
  Form,
  Input,
  InputGroupAddon,
  InputGroupText,
  InputGroup,
  Container,
  Row,
  Col,
  Alert
} from "reactstrap";
import { Redirect } from 'react-router-dom';

import * as Appconst from '../../Appcosnt';
import background_image from "../../assets/img/loginscreen_background.jpg";

class Registration extends React.Component {

  constructor(props) {
    super(props)

    this.state = {
      fields: {},
      errors: {},
      room: '',
      errorAlert: 'none',
      errormassage: '',
      successmassage: '',
      successAlert: 'none',
    }
  }


  componentDidMount() {
    document.documentElement.scrollTop = 0;
    document.scrollingElement.scrollTop = 0;
    this.refs.main.scrollTop = 0;
    let paramId = this.props.match.params.roomId;

    this.setState({ room: paramId })


  }

  handleValidation() {
    let fields = this.state.fields;
    let errors = {};
    let formIsValid = true;

    //Name
    if (!fields["name"]) {
      formIsValid = false;
      errors["name"] = "Cannot be empty";
    }

    if (typeof fields["name"] !== "undefined") {
      if (!fields["name"].match(/^[a-zA-Z]+$/)) {
        formIsValid = false;
        errors["name"] = "Only letters";
      }
    }

    //Email
    if (!fields["email"]) {
      formIsValid = false;
      errors["email"] = "Cannot be empty";
    }

    if (typeof fields["email"] !== "undefined") {
      let lastAtPos = fields["email"].lastIndexOf('@');
      let lastDotPos = fields["email"].lastIndexOf('.');

      if (!(lastAtPos < lastDotPos && lastAtPos > 0 && fields["email"].indexOf('@@') == -1 && lastDotPos > 2 && (fields["email"].length - lastDotPos) > 2)) {
        formIsValid = false;
        errors["email"] = "Email is not valid";
      }
    }

    if (!fields["password"]) {
      formIsValid = false;
      errors["password"] = "Cannot be empty";
    }

    this.setState({ errors: errors });
    return formIsValid;
  }

  handleChange(field, e) {
    let fields = this.state.fields;
    fields[field] = e.target.value;
    this.setState({ fields });
  }

  submit() {
    if (this.handleValidation()) {
      this.senddata();
    } else {

    }
  }

  senddata() {
    let fields = this.state.fields;

    fetch(Appconst.REGISTRATION, {//otp sent route
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        "name": fields["name"],
        "email": fields["email"],
        "password": fields["password"]

      }),
    })
      .then((response) => response.json())
      .then((responseJson) => {
        console.log('response', responseJson)
        if (responseJson.success == true) {


          let roomId = this.state.room;

          if (roomId != undefined) {
            this.verificationAlert();

            setTimeout(function () {
              this.props.history.push('/login/' + roomId);
            }.bind(this), 2100)

          } else {
            this.verificationAlert();

            setTimeout(function () {
              this.props.history.push('/login');
            }.bind(this), 2100)
          }


        } else {

          this.setState({
            errorAlert: 'flex',
            errormassage: responseJson.message
          })

          setTimeout(function () { //Start the timer
            this.setState({
              errorAlert: 'none',
              errormassage: ''
            })
          }.bind(this), 2000)
        }
      })
      .catch((error) => {

      });
  }

  verificationAlert() {
    this.setState({
      successAlert: 'flex',
      successmassage: "Please verify your email address"
    })

    setTimeout(function () { //Start the timer
      this.setState({
        successAlert: 'none',
        successmassage: ''
      })
    }.bind(this), 2000)
  }


  render() {
    return (
      <>

        <main class="login_main" ref="main" style={{ backgroundImage: `url(${background_image})` }}>
          <Alert style={{ position: 'fixed', width: '100%', zIndex: 2, display: this.state.errorAlert }} color="danger">
            {this.state.errormassage}
          </Alert>

          <Alert style={{ position: 'fixed', width: '100%', zIndex: 2, display: this.state.successAlert }} color="success">
            {this.state.successmassage}
          </Alert>

          <section className="section section-shaped " style={{ backgroundColor: 'rgba(0, 0, 0, 0.53)', height: '116%' }}>

            <Container >
              <Row className="justify-content-center">
                <Col lg="5">
                  <Card id="login_form" className="bg-secondary shadow border-0">
                    <CardHeader className="pb-0" style={{ backgroundColor: 'transparent' }}>
                      <div className="text-muted text-center mb-3">
                        <small className="textOn_wood">Sign in with</small>
                      </div>
                      <div className="btn-wrapper text-center">
                        <Button
                          className="btn-neutral btn-icon transparent_button"
                          color="default"
                          href="#pablo"
                          onClick={e => e.preventDefault()}
                        >
                          <span className="btn-inner--icon mr-1">
                            <img
                              alt="..."
                              src={require("./../../assets/img/icons/common/github.svg")}
                            />
                          </span>
                          <span className="btn-inner--text">Github</span>
                        </Button>
                        <Button
                          className="btn-neutral btn-icon ml-1 transparent_button"
                          color="default"
                          href="#pablo"
                          onClick={e => e.preventDefault()}
                        >
                          <span className="btn-inner--icon mr-1">
                            <img
                              alt="..."
                              src={require("./../../assets/img/icons/common/google.svg")}
                            />
                          </span>
                          <span className="btn-inner--text">Google</span>
                        </Button>
                      </div>
                    </CardHeader>
                    <CardBody className="px-lg-5 py-lg-5">
                      <div className="text-center text-muted mb-4">
                        <small className="textOn_wood">Or sign in with credentials</small>
                      </div>
                      <Form role="form">



                        <FormGroup className="mb-3">

                          <InputGroup className="input-group-alternative">
                            <InputGroupAddon addonType="prepend">
                              <InputGroupText className="transparent_button">
                                <i className="ni ni-circle-08" />
                              </InputGroupText>
                            </InputGroupAddon>
                            <Input placeholder="Display Name" maxLength={10} type="name" className="transparent_button"

                              onChange={e => this.handleChange("name", e)}
                              value={this.state.fields["name"]}
                            />

                          </InputGroup>
                          <span className="validation_error">{this.state.errors["name"]}</span>
                        </FormGroup>

                        <FormGroup >
                          <InputGroup className="input-group-alternative">
                            <InputGroupAddon addonType="prepend">
                              <InputGroupText className="transparent_button">
                                <i className="ni ni-email-83" />
                              </InputGroupText>
                            </InputGroupAddon>
                            <Input placeholder="Email" type="email" className="transparent_button"
                              onChange={e => this.handleChange("email", e)}
                              value={this.state.fields["email"]} />
                          </InputGroup>
                          <span className="validation_error">{this.state.errors["email"]}</span>
                        </FormGroup>
                        <FormGroup>
                          <InputGroup className="input-group-alternative">
                            <InputGroupAddon addonType="prepend">
                              <InputGroupText className="transparent_button">
                                <i className="ni ni-lock-circle-open" />
                              </InputGroupText>
                            </InputGroupAddon>
                            <Input
                              className="transparent_button"
                              placeholder="Password"
                              type="password"
                              autoComplete="off"
                              onChange={e => this.handleChange("password", e)}
                              value={this.state.fields["password"]} />

                          </InputGroup>
                          <span className="validation_error">{this.state.errors["password"]}</span>
                        </FormGroup>

                        <div className="text-center">
                          <Button
                            className="my-3 transparent_button"
                            onClick={() => this.submit()}
                            type="button"
                          >
                            Sign Up
                          </Button>
                        </div>
                      </Form>


                    </CardBody>
                  </Card>

                </Col>
              </Row>

            </Container>

          </section>
        </main>

      </>
    );
  }
}

export default Registration;
