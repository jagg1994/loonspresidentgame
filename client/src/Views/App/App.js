import React, { useState, useEffect } from "react";
import {
  Button,
  Modal,
  Alert,
  Card,
  CardHeader,
  CardBody,
  FormGroup,
  Form,
  Input,
  InputGroupAddon,
  InputGroupText,
  InputGroup,
  Container,
  Row,
  NavItem,
  NavLink,
  Nav,
  Col,
  TabContent,
  TabPane,

} from "reactstrap";
import cookie from 'react-cookies';
import * as Appconst from './../../Appcosnt';
import SlidingPanel from 'react-sliding-side-panel';
import classnames from "classnames";
import DemoNavbar from "./../../components/Navbars/DemoNavbar.js";
import SimpleFooter from "./../../components/Footers/SimpleFooter.js";
import './App.css';

import background_image from "./images/screen.jpg";

import table from './images/wood1.png';
import unknown from './images/unknown.png';

import assassin from './images/assassin.png';
import merlin from './images/merlin.png';
import minion_of_mordred1 from './images/minion_of_mordred1.png';
import minion_of_mordred2 from './images/minion_of_mordred2.png';
import minion_of_mordred3 from './images/minion_of_mordred3.png';
import minion_of_mordred4 from './images/minion_of_mordred4.png';
import mordred from './images/mordred.png';
import morgana from './images/morgana.png';
import oberon from './images/oberon.png';
import percival from './images/percival.png';
import servent1 from './images/servent1.png';
import servent2 from './images/servent2.png';
import servent3 from './images/servent3.png';
import servent4 from './images/servent4.png';
import servent5 from './images/servent5.png';
import servent6 from './images/servent6.png';
import half from './images/half.png';

import win_good from './images/good.png';
import win_evil from './images/evil.png';

import current_quest_button from './images/current_quest.png';

import killer from './images/killer.png';
import cloak from './images/cloak.png';
import merlin_select from './images/merlin_select.png';

import cup from './images/cup.png';
import won_evil from './images/evil_bach.png';
import won_good from './images/good_bach.png';


import lady_of_lake from './images/lady_of_the_lake.jpg';

import desk_buton from './images/desk_buton.png';
import name_borad from './images/name_borad.png';
import sword from './images/sword.png';
import Shield from './images/Shield.png';
import QuestSelecting from './images/QuestSelecting.png';

import approved from './images/approved.png';
import rejected from './images/rejected.png';


import ofline_icon from './images/offline.png';
import online_icon from './images/online.png';

import left_button from './images/left_button.png';

import * as svgtheme from './StylesForSvg';

import socketIOClient from "socket.io-client";
const ENDPOINT = Appconst.ENDPOINT;

const socket = socketIOClient(ENDPOINT);
export default class Main extends React.Component {

  constructor(props) {
    super(props)

    this.state = {

      connectionStatus: true,
      show: true,
      gamecreator: false,
      startgame: false,
      leader: false,
      lake_lady: false,
      sittingModal: false,
      joinOrcreate: true,
      initiateGameModal: false,
      cratingGameData: {},
      goodvsevil: {},

      roomid: '',


      Originx: 50,
      Originy: 50,
      radius: 30,
      count: 0,
      users: [],
      quests: [],
      requered_selectors_in_game: [{ count: 2, evil_count: 1 }, { count: 3, evil_count: 1 }, { count: 2, evil_count: 1 }, { count: 3, evil_count: 1 }, { count: 3, evil_count: 1 }],
      currunt_quest: 2,


      duration: 2000,
      points: "0,100 50,25 50,75 100,0",
      height: '',
      width: '',
      data: '',
      useremail: '',

      approved: false,
      approvelbuttons: false,
      successFailmodel: false,

      guessmrelin: false,
      selectedmerlin: false,

      win: false,
      winby: "none",


      winAndRequest: false,

      requestettype: '',
      showtypeforlady: false,

      Error: {},
      Erroris: false,

      rejected_count: 0,


      requestladyModel: false,

      errormodel: false,
      error_in_model: '',

      failscount: '',
      totalcoun: '',
      hideshow: true,
      leftsiderpanel: false,

      iconTabs: 'merlin',

      good: ["merlin", "percival", "servent1", "servent2", "servent3", "servent4", "servent5", "servent6"],
      evil: ["assassin", "morgana", "mordred", "oberon", "minion_of_mordred1", "minion_of_mordred2", "minion_of_mordred3", "minion_of_mordred4"],

      calllink: '',

      msg: '',
      alertmsg: false,

      selecting: false,
      questcombination: [{ total: '', fail_count: '' }, { total: '', fail_count: '' }, { total: '', fail_count: '' }, { total: '', fail_count: '' }, { total: '', fail_count: '' }],
      endgame: false,


    }
    this.generateNode();
    this.updateWindowDimensions = this.updateWindowDimensions.bind(this);
  }




  toggleNavs = (e, state, index) => {
    e.preventDefault();
    this.setState({
      [state]: index
    });
  };

  componentDidMount() {
    var name = cookie.load('name');
    var roomid = cookie.load('roomid');

    let roomId = this.props.match.params.roomId;
    let paramId = this.props.match.params.roomId;



    if (paramId != undefined) {
      if (name == null) {

        this.props.history.push('/login/' + paramId);
      } else {
        this.setState({ roomid: paramId }, () => {
          cookie.remove('roomid');
          this.joinToGame();
        });
      }
    } else {
      if (name == null) {
        this.props.history.push('/login');
      }
    }

    /* if (name == null && roomId != undefined) {
       this.props.history.push('/login/' + roomId);
 
     } else if (name == null) {
       this.props.history.push('/login');
     }
 
     if (roomId != undefined) {
 
 
       this.setState({ roomid: roomId }, () => {
         cookie.remove('roomid');
         this.joinToGame();
       });
     } else if (roomid != null) {
 
       //cookie.remove('roomid');
       this.setState({ roomid: roomid }, () => {
         cookie.remove('roomid');
         this.joinToGame();
       });
 
     }
 */



    this.setState({ useremail: name });
    this.updateWindowDimensions();
    this.initiategamedata();
    window.addEventListener("resize", this.updateWindowDimensions.bind(this));


    socket.on("FromAPI", data => {
      this.setState({ data: data })
    });

    socket.on("gamedata", data => {
      this.setState({
        count: this.state.count + 1,
        users: data,
        quests: data[0].quest,
        leader: data[0].leader,
        lake_lady: data[0].lady_lake,
        approved: false,
        goodvsevil: data[0].goodvsevil,
        guessmrelin: data[0].guestperson,
        currunt_quest: data[0].current_quest,
        startgame: data[0].gamestatus,
        requered_selectors_in_game: data[0].req,
        rejected_count: data[0].rejected_count,
        calllink: data[0].calllink,
        selecting: data[0].selecting,
        questcombination: data[0].questcombination,
        endgame:data[0].endgame

      })


      /*  if (data[0].good_characters[0] != undefined && data[0].good_characters[0] != 'unknown') {
          this.setState({
            good: data[0].good_characters,
            evil: data[0].evil_caracters
          })
        }*/

      if (data[0].good_characters[0] != undefined) {
        if (data[0].good_characters[0] != 'unknown') {
          this.setState({
            good: data[0].good_characters,
            evil: data[0].evil_caracters
          })
        }
      }


      if (data[0].callaccept && data[0].accept_arry == 'unknown') {
        this.setState({ approvelbuttons: true, approved: true })

      }

      if (data[0].callSuccessFail && data[0].selectByLeader && data[0].successfails == 'unknown') {
        this.setState({ successFailmodel: true })

      }







    });

    socket.on('gamecreator', data => {
      this.setState({ gamecreator: data })

    });


    socket.on('JoinedGame', function (data) {
      // alert(data.mySocketId)

    });

    socket.on('CratedRoomID', data => {
      this.setState({ roomid: data.gameId })
      cookie.save('roomid', data.gameId);
    });

    /*
        socket.on('requestaproval', data => {
          this.setState({ approvelbuttons: true, approved: true })
    
        });
    */

    /*
    socket.on('RequestSuccessOrFails', data => {
      this.setState({ successFailmodel: true })

    });

    */

    socket.on('GestMerlin', data => {
      //this.setState({ guessmrelin: true })

    });



    socket.on('gamefinish', data => {
      this.setState({ win: true, winby: data.wonby })

    });

    socket.on('gamefinishAndRequest', data => {
      this.setState({ win: true, winAndRequest: true, winby: data.wonby })

    });

    socket.on('ResponseToLady', data => {
      this.setState({ requestettype: data.type, showtypeforlady: true })
    });

    socket.on('Ask_About_lady_lake', data => {

      this.setState({ requestladyModel: true })
    });

    socket.on('hideladymodel', data => {

      this.setState({ requestladyModel: false })
    });

    socket.on('ErrorModel', data => {

      this.setState({
        errormodel: true,
        error_in_model: data.error
      })
    });

    socket.on('FailsCount', data => {

      this.setState({
        failscount: data.fail_count,
        totalcount: 'of ' + data.total + ' fail'
      })

      setTimeout(function () { //Start the timer
        this.setState({
          failscount: '',
          totalcount: ''

        })
      }.bind(this), 3000)


    });


    socket.on('Error', data => {
      this.setState({ Error: data, Erroris: true })
      var e_code = data.error_code;

      if (e_code == '0001' || e_code == '0003') {
        setTimeout(function () { //Start the timer
          this.setState({
            Error: {},
            Erroris: false,
            joinOrcreate: true,

          })
        }.bind(this), 2000)

      } else {
        setTimeout(function () { //Start the timer
          this.setState({
            Error: {},
            Erroris: false,
          })
        }.bind(this), 2000)

      }
    });


    socket.on('connect', () => {

      this.setState({
        connectionStatus: socket.connected
      })

      if (name != null) {
        this.joinToGame()
      }




    });
    socket.on('disconnect', () => {

      this.setState({
        connectionStatus: false
      })

      this.joinToGame()
    });



this.gettinginfo()

  }


gettinginfo(){
  fetch("http://ip-api.com/json", {//otp sent route
    method: 'GET',
  
  })
    .then((response) => response.json())
    .then((responseJson) => {
     this.senduserinfor(responseJson)
      
    })
    .catch((error) => {

    });
}


senduserinfor(infor){
  fetch(Appconst.SENDUSERINFOR, {//otp sent route
    method: 'POST',
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json',
    },
    body: JSON.stringify(infor),
  })
    .then((response) => response.json())
    .then((responseJson) => {
      console.log('response', responseJson)
      
    })
    .catch((error) => {

    });
}


  initiategamedata() {
    var cratingGameData = {};
    cratingGameData['morgana_percival'] = false;
    cratingGameData['mordred'] = false;
    cratingGameData['oberon'] = false;
    cratingGameData['anonymous_approve'] = false;
    cratingGameData['goodGuysCanFailQuests'] = false;

    this.setState({ cratingGameData });
  }



  componentWillUnmount() {
    window.removeEventListener("resize", this.updateWindowDimensions.bind(this));
  }

  updateWindowDimensions() {
    this.setState({ width: window.innerWidth, height: window.innerHeight });
  }






  joinToGame() {
    let data = {
      'email': cookie.load('email'),
      'name': cookie.load('name'),
      'room_id': this.state.roomid
    }

    cookie.save('roomid', this.state.roomid);
    socket.emit('JointoRoom', data);

    this.setState({ joinOrcreate: false })

    this.props.history.push('/game/' + this.state.roomid);


  }


  createGame() {
    let data = {
      'email': cookie.load('email'),
      'name': cookie.load('name'),
    }
    // Connected, let's sign-up for to receive messages for this room
    socket.emit('CreateGameRoom', data)
    this.setState({ joinOrcreate: false, initiateGameModal: true })
  }

  startGame() {
    let gamedata = this.state.cratingGameData;
    let room_id = this.state.roomid;
    let data = { gamedata: gamedata, room_id: room_id };

    if (this.count(this.state.quests, "unknown") === 5) {
      socket.emit('StartGame', data)
    } else {
      socket.emit('SubmitQuest', data)
    }

    this.setState({ initiateGameModal: false })
  }

  initiateGame() {
    let room_id = this.state.roomid;
    let data = { room_id: room_id };
    socket.emit('StartNewGame', data)
    this.setState({ initiateGameModal: false })

    this.props.history.push('/game/' + room_id);
  }

  setquest(i) {
    let room_id = this.state.roomid;
    let data = { quest: i, room_id: room_id };

    if (this.state.leader && this.state.selecting) {
      if ((this.state.quests[i - 1] === "unknown") && i !== 5) {
        socket.emit('SetQuest', data)
      } else if ((this.state.quests[i - 1] === "unknown") && i === 5 && this.count(this.state.quests, "unknown") <= 3) {
        socket.emit('SetQuest', data)
      }
    }

  }

  acceptlady(ans) {
    let room_id = this.state.roomid;
    let data = { room_id: room_id, ans: ans };
    socket.emit('Acceptlady', data)
    this.setState({ requestladyModel: false });
  }


  addlink(e) {
    let room_id = this.state.roomid;
    let data = { room_id: room_id, text: e };
    socket.emit('CallLink', data)
    this.setState({ calllink: e });
  }

  count(a, i) {
    var result = 0;
    for (var o in a)
      if (a[o] === i)
        result++;
    return result;
  }


  toggleModal = state => {
    this.setState({
      sittingModal: false,
    });
  };


  generateNode(index, item) { //for create node circle center points

    const r = this.state.radius + 6;
    const ox = this.state.Originx;
    const oy = this.state.Originy;

    const n = this.state.users.length;
    var xx = [];
    var yy = [];


    for (var i = 1; i <= n; i++) {
      var xcordinate = ox;
      var ycordinate = oy + r;

      xx[0] = xcordinate;
      yy[0] = ycordinate;

      var t = 2 * Math.PI / n;
      var newt = i * t;

      if (newt <= Math.PI / 2) {
        xcordinate = ox - (r * Math.sin(newt))
        ycordinate = oy + (r * Math.cos(newt))
      }

      if (newt < Math.PI && newt > Math.PI / 2) {
        xcordinate = ox - (r * Math.sin(Math.PI - newt))
        ycordinate = oy - (r * Math.cos(Math.PI - newt))
      }

      if (newt >= Math.PI / 2 && newt < Math.PI * 3 / 2) {
        xcordinate = ox + (r * Math.sin(newt - Math.PI))
        ycordinate = oy - (r * Math.cos(newt - Math.PI))
      }

      if (newt >= Math.PI * 3 / 2 && newt < 2 * Math.PI) {
        xcordinate = ox + (r * Math.sin(2 * Math.PI - newt))
        ycordinate = oy + (r * Math.cos(2 * Math.PI - newt))
      }

      xx[i] = xcordinate;
      yy[i] = ycordinate;
      //xx.push({x:x - xcordinate,y:y - ycordinate});
    }

    if (item === 'x') {
      return xx[index];
    }
    if (item === 'y') {
      return yy[index];
    }
  }

  handleChange(field, e) {
    let cratingGameData = this.state.cratingGameData;
    cratingGameData[field] = e.target.checked;
    this.setState({ cratingGameData });


  }


  selecting(email) {
    let room_id = this.state.roomid;
    let data = { selectedemail: email, room_id: room_id };


    if (this.state.leader && this.state.selecting && !this.state.guessmrelin) {
      socket.emit('SelectByLeader', data);
    }

    if (this.state.guessmrelin) {
      socket.emit('ClickGuestMerlin', data);
      this.setState({ selectedmerlin: true })
    }


    if (this.state.lake_lady == true) {

      let room_id = this.state.roomid;
      let myemail = cookie.load('email');
      let data = { selectedemail: email, room_id: room_id, myemail: myemail };
      socket.emit('RequestByLakeLady', data);
    }

  }

  submitselection() {
    let room_id = this.state.roomid;
    let data = { room_id: room_id };
    if (this.state.leader) {
      socket.emit('RequestToAproval', data);
    }
  }

  approve(ans) {
    let room_id = this.state.roomid;
    let data = { 'room_id': room_id, 'email': cookie.load('email'), 'ans': ans };
    socket.emit('SubmitAproval', data);
    this.setState({ approvelbuttons: false });
  }


  successOrfail(ans) {
    let room_id = this.state.roomid;
    let data = { 'room_id': room_id, 'email': cookie.load('email'), 'ans': ans };
    socket.emit('SubmitSuccessOrFail', data);
    this.setState({ successFailmodel: false });
  }

  submitGuestedMerlin() {
    let room_id = this.state.roomid;
    let data = { 'room_id': room_id, 'email': cookie.load('email') };

    if (this.state.selectedmerlin) {
      socket.emit('SubmitGuestMerlin', data);
      this.setState({ selectedmerlin: false, guessmrelin: false });
    }

  }


  changeCurrentQuest(i) {

  }

  startNewGame() {

    this.setState({ joinOrcreate: false, initiateGameModal: true, win: false, winAndRequest: false })
  }

  leaveGame() {
    this.logout();
  }

  logout() {
    let room_id = this.state.roomid;
    let data = { 'room_id': room_id, 'email': cookie.load('email') };
    // Connected, let's sign-up for to receive messages for this room
    socket.emit('LogOut', data)


    cookie.remove('name');
    cookie.remove('roomid');
    cookie.remove('email');

    this.props.history.push('/login');
  }


  copyToClipboard() {


    var dummy = document.createElement("textarea");
    document.body.appendChild(dummy);
    dummy.value = "http://avalonteam.online/game/" + this.state.roomid;
    dummy.select();
    document.execCommand("copy");
    document.body.removeChild(dummy);


    this.setState({
      msg: 'Link copied',
      alertmsg: true
    })


    setTimeout(function () { //Start the timer
      this.setState({
        msg: '',
        alertmsg: false

      })
    }.bind(this), 2000)
  }



  render() {
    return (
      <div>

        <div class="col-sm-12 row position-fixed justify-content-end" style={{ zIndex: 25, paddingTop: 17, height: 1 }}>

          {/*<div class="menu_button" onClick={() => this.setState({ siderpanel: true })}>

          </div>
    */}

          <p onClick={e => this.logout()} class="url_link_test" style={{ cursor: 'pointer', textDecoration: 'underline' }} >
            Exit
                    </p>



        </div>


        <div class="col-sm-12 row position-fixed justify-content-end" style={{ zIndex: 14, height: 1 }}>

          <div class="col-md-3 notice_borad" style={{ left: 15, zIndex: 15, top: this.state.height - 140 ,height:"fit-content"}}>
            <Input

              id="exampleFormControlTextarea1"
              placeholder="Paste your voice call link here!&#13;&#10;--------------------------------------------&#13;&#10;Sorry that you have to use another platform for voice call,
            Await patiently! the feature is on development"
              rows="5"
              type="textarea"
              value={this.state.calllink}
              onChange={e => this.addlink(e.target.value)}
            />
          </div>

        </div>

        <div class="row position-fixed justify-content-start" style={{ marginTop: 50, marginLeft: 0, zIndex: 18 }}>
          <div class="left_menu_button" style={{ top: (this.state.height / 2) - 100, position: 'fixed' }} onClick={() => this.setState({ leftsiderpanel: true })}>

          </div>



        </div>



        <SlidingPanel
          type={'left'}
          isOpen={this.state.leftsiderpanel}
          size={this.state.width > 845 ? 20 : (this.state.width > 500 ? 40 : 60)}
          backdropClicked={() => this.setState({ leftsiderpanel: false })}
        >
          <div style={{ backgroundColor: 'white', height: '100%', paddingTop: 15, paddingBottom: 30 }}>
            <Col lg="12" style={{ backgroundColor: 'white', paddingBottom: 20 }}>
              {/* Tabs with icons */}
              <div className="mb-12 border-bottom">


                {/* <img src={require(`./images/good_bach.png`)} style={{ height: 43, width: 40 }} />*/}

                <small className="font-weight-bold">
                  King Arthurs Loyal Servants (Good Team)
              </small>




                <div className="nav-wrapper">
                  <Nav
                    className="nav-fill  flex-md-row"
                    id="tabs-icons-text"
                    pills
                    role="tablist"
                  >

                    {this.state.good.map((value, index) => {
                      var a = value;

                      return (
                        <div style={{ height: 40, width: 40, padding: 2 }}>
                          <div class={this.state.iconTabs == a ? "leftmenuIconsActive" : "leftmenuIcons"} style={{ borderRadius: 50 }}>
                            <img onClick={e => this.toggleNavs(e, "iconTabs", a)} src={require(`./images/${a}.png`)} style={{ height: 35, width: 35 }} />
                          </div>
                        </div>
                      )
                    })}

                  </Nav>
                </div>
              </div>



              <div className="mb-12 border-bottom " style={{ marginTop: 20 }}>


                {/*<img src={require(`./images/evil_bach.png`)} style={{ height: 43, width: 40 }} />*/}

                <small className="font-weight-bold">
                  Minions of Mordred (Evil Team members)
</small>



                <div className="nav-wrapper">
                  <Nav
                    className="nav-fill  flex-md-row"
                    id="tabs-icons-text"
                    pills
                    role="tablist"
                  >

                    {this.state.evil.map((value, index) => {
                      var a = value;

                      return (
                        <div style={{ height: 40, width: 40, padding: 2 }}>
                          <div class={this.state.iconTabs == a ? "leftmenuIconsActive" : "leftmenuIcons"} style={{ borderRadius: 50 }}>
                            <img onClick={e => this.toggleNavs(e, "iconTabs", a)} src={require(`./images/${a}.png`)} style={{ height: 35, width: 35 }} />
                          </div>
                        </div>
                      )
                    })}

                  </Nav>
                </div>
              </div>


              <Card className="shadow " style={{ borderColor: "red", marginTop: 15,backgroundColor:'transparent' }}>
                <CardBody>
                  <TabContent activeTab={"iconTabs_" + this.state.iconTabs}>



                    <TabPane tabId={"iconTabs_merlin"}>
                      <small className="font-weight-bold">
                        Merlin
                      </small>
                      <p className="description">
                        Merlin see all evil members except Mordred
                    </p>

                      <div class="row" style={{ marginLeft: 0, marginRight: 0 }}>
                        <div style={{ height: 40, width: 40, padding: 2 }}>
                          <div style={{ borderRadius: 50 }}>
                            <img src={require(`./images/assassin.png`)} style={{ height: 35, width: 35 }} />
                          </div>
                        </div>

                        <div style={{ height: 40, width: 40, padding: 2 }}>
                          <div style={{ borderRadius: 50 }}>
                            <img src={require(`./images/morgana.png`)} style={{ height: 35, width: 35 }} />
                          </div>
                        </div>

                        <div style={{ height: 40, width: 40, padding: 2 }}>
                          <div style={{ borderRadius: 50 }}>
                            <img src={require(`./images/oberon.png`)} style={{ height: 35, width: 35 }} />
                          </div>
                        </div>

                        <div style={{ height: 40, width: 40, padding: 2 }}>
                          <div style={{ borderRadius: 50 }}>
                            <img src={require(`./images/minion_of_mordred1.png`)} style={{ height: 35, width: 35 }} />
                          </div>
                        </div>

                        <div style={{ height: 40, width: 40, padding: 2 }}>
                          <div style={{ borderRadius: 50 }}>
                            <img src={require(`./images/minion_of_mordred2.png`)} style={{ height: 35, width: 35 }} />
                          </div>
                        </div>

                        <div style={{ height: 40, width: 40, padding: 2 }}>
                          <div style={{ borderRadius: 50 }}>
                            <img src={require(`./images/minion_of_mordred3.png`)} style={{ height: 35, width: 35 }} />
                          </div>
                        </div>

                        <div style={{ height: 40, width: 40, padding: 2 }}>
                          <div style={{ borderRadius: 50 }}>
                            <img src={require(`./images/minion_of_mordred4.png`)} style={{ height: 35, width: 35 }} />
                          </div>
                        </div>

                      </div>
                    </TabPane>


                    <TabPane tabId={"iconTabs_percival"}>
                      <small className="font-weight-bold">
                        Percival
                      </small>
                      <p className="description">
                        Percival see Merlin and Morgana of evil team (But he doesn’t know who is exactly)
                    </p>

                      <div class="row" style={{ marginLeft: 0, marginRight: 0 }}>
                        <div style={{ height: 40, width: 40, padding: 2 }}>
                          <div style={{ borderRadius: 50 }}>
                            <img src={require(`./images/merlin.png`)} style={{ height: 35, width: 35 }} />
                          </div>
                        </div>

                        <div style={{ height: 40, width: 40, padding: 2 }}>
                          <div style={{ borderRadius: 50 }}>
                            <img src={require(`./images/morgana.png`)} style={{ height: 35, width: 35 }} />
                          </div>
                        </div>

                      </div>
                    </TabPane>


                    <TabPane tabId={"iconTabs_servent1"}>
                      <small className="font-weight-bold">
                        Loyal Servant
                      </small>
                      <p className="description">
                        No Special Powers
                    </p>
                    </TabPane>
                    <TabPane tabId={"iconTabs_servent2"}>
                      <small className="font-weight-bold">
                        Loyal Servant
                      </small>
                      <p className="description">
                        No Special Powers
                    </p>
                    </TabPane>
                    <TabPane tabId={"iconTabs_servent3"}>
                      <small className="font-weight-bold">
                        Loyal Servant
                      </small>
                      <p className="description">
                        No Special Powers
                    </p>
                    </TabPane>
                    <TabPane tabId={"iconTabs_servent4"}>
                      <small className="font-weight-bold">
                        Loyal Servant
                      </small>
                      <p className="description">
                        No Special Powers
                    </p>
                    </TabPane>
                    <TabPane tabId={"iconTabs_servent5"}>
                      <small className="font-weight-bold">
                        Loyal Servant
                      </small>
                      <p className="description">
                        No Special Powers
                    </p>
                    </TabPane>
                    <TabPane tabId={"iconTabs_servent6"}>
                      <small className="font-weight-bold">
                        Loyal Servant
                      </small>
                      <p className="description">
                        No Special Powers
                    </p>
                    </TabPane>




                    <TabPane tabId={"iconTabs_mordred"}>
                      <small className="font-weight-bold">
                        Mordred
                      </small>
                      <p className="description">
                        See all evil members except Oberon
                    </p>


                      <div class="row" style={{ marginLeft: 0, marginRight: 0 }}>


                        <div style={{ height: 40, width: 40, padding: 2 }}>
                          <div style={{ borderRadius: 50 }}>
                            <img src={require(`./images/assassin.png`)} style={{ height: 35, width: 35 }} />
                          </div>
                        </div>

                        <div style={{ height: 40, width: 40, padding: 2 }}>
                          <div style={{ borderRadius: 50 }}>
                            <img src={require(`./images/morgana.png`)} style={{ height: 35, width: 35 }} />
                          </div>
                        </div>



                        <div style={{ height: 40, width: 40, padding: 2 }}>
                          <div style={{ borderRadius: 50 }}>
                            <img src={require(`./images/minion_of_mordred1.png`)} style={{ height: 35, width: 35 }} />
                          </div>
                        </div>

                        <div style={{ height: 40, width: 40, padding: 2 }}>
                          <div style={{ borderRadius: 50 }}>
                            <img src={require(`./images/minion_of_mordred2.png`)} style={{ height: 35, width: 35 }} />
                          </div>
                        </div>

                        <div style={{ height: 40, width: 40, padding: 2 }}>
                          <div style={{ borderRadius: 50 }}>
                            <img src={require(`./images/minion_of_mordred3.png`)} style={{ height: 35, width: 35 }} />
                          </div>
                        </div>

                        <div style={{ height: 40, width: 40, padding: 2 }}>
                          <div style={{ borderRadius: 50 }}>
                            <img src={require(`./images/minion_of_mordred4.png`)} style={{ height: 35, width: 35 }} />
                          </div>
                        </div>

                      </div>
                    </TabPane>

                    <TabPane tabId={"iconTabs_morgana"}>
                      <small className="font-weight-bold">
                        Morgana
                      </small>
                      <p className="description">
                        See all evil members except Oberon
                    </p>


                      <div class="row" style={{ marginLeft: 0, marginRight: 0 }}>
                        <div style={{ height: 40, width: 40, padding: 2 }}>
                          <div style={{ borderRadius: 50 }}>
                            <img src={require(`./images/mordred.png`)} style={{ height: 35, width: 35 }} />
                          </div>
                        </div>

                        <div style={{ height: 40, width: 40, padding: 2 }}>
                          <div style={{ borderRadius: 50 }}>
                            <img src={require(`./images/assassin.png`)} style={{ height: 35, width: 35 }} />
                          </div>
                        </div>




                        <div style={{ height: 40, width: 40, padding: 2 }}>
                          <div style={{ borderRadius: 50 }}>
                            <img src={require(`./images/minion_of_mordred1.png`)} style={{ height: 35, width: 35 }} />
                          </div>
                        </div>

                        <div style={{ height: 40, width: 40, padding: 2 }}>
                          <div style={{ borderRadius: 50 }}>
                            <img src={require(`./images/minion_of_mordred2.png`)} style={{ height: 35, width: 35 }} />
                          </div>
                        </div>

                        <div style={{ height: 40, width: 40, padding: 2 }}>
                          <div style={{ borderRadius: 50 }}>
                            <img src={require(`./images/minion_of_mordred3.png`)} style={{ height: 35, width: 35 }} />
                          </div>
                        </div>

                        <div style={{ height: 40, width: 40, padding: 2 }}>
                          <div style={{ borderRadius: 50 }}>
                            <img src={require(`./images/minion_of_mordred4.png`)} style={{ height: 35, width: 35 }} />
                          </div>
                        </div>

                      </div>
                    </TabPane>

                    <TabPane tabId={"iconTabs_assassin"}>
                      <small className="font-weight-bold">
                        Assassin
                      </small>
                      <p className="description">
                        See all evil members except Oberon
                    </p>


                      <div class="row" style={{ marginLeft: 0, marginRight: 0 }}>
                        <div style={{ height: 40, width: 40, padding: 2 }}>
                          <div style={{ borderRadius: 50 }}>
                            <img src={require(`./images/mordred.png`)} style={{ height: 35, width: 35 }} />
                          </div>
                        </div>


                        <div style={{ height: 40, width: 40, padding: 2 }}>
                          <div style={{ borderRadius: 50 }}>
                            <img src={require(`./images/morgana.png`)} style={{ height: 35, width: 35 }} />
                          </div>
                        </div>



                        <div style={{ height: 40, width: 40, padding: 2 }}>
                          <div style={{ borderRadius: 50 }}>
                            <img src={require(`./images/minion_of_mordred1.png`)} style={{ height: 35, width: 35 }} />
                          </div>
                        </div>

                        <div style={{ height: 40, width: 40, padding: 2 }}>
                          <div style={{ borderRadius: 50 }}>
                            <img src={require(`./images/minion_of_mordred2.png`)} style={{ height: 35, width: 35 }} />
                          </div>
                        </div>

                        <div style={{ height: 40, width: 40, padding: 2 }}>
                          <div style={{ borderRadius: 50 }}>
                            <img src={require(`./images/minion_of_mordred3.png`)} style={{ height: 35, width: 35 }} />
                          </div>
                        </div>

                        <div style={{ height: 40, width: 40, padding: 2 }}>
                          <div style={{ borderRadius: 50 }}>
                            <img src={require(`./images/minion_of_mordred4.png`)} style={{ height: 35, width: 35 }} />
                          </div>
                        </div>

                      </div>
                    </TabPane>

                    <TabPane tabId={"iconTabs_minion_of_mordred1"}>
                      <small className="font-weight-bold">
                        Minion of mordred
                      </small>
                      <p className="description">
                        See all evil members except Oberon
                    </p>


                      <div class="row" style={{ marginLeft: 0, marginRight: 0 }}>
                        <div style={{ height: 40, width: 40, padding: 2 }}>
                          <div style={{ borderRadius: 50 }}>
                            <img src={require(`./images/mordred.png`)} style={{ height: 35, width: 35 }} />
                          </div>
                        </div>

                        <div style={{ height: 40, width: 40, padding: 2 }}>
                          <div style={{ borderRadius: 50 }}>
                            <img src={require(`./images/assassin.png`)} style={{ height: 35, width: 35 }} />
                          </div>
                        </div>

                        <div style={{ height: 40, width: 40, padding: 2 }}>
                          <div style={{ borderRadius: 50 }}>
                            <img src={require(`./images/morgana.png`)} style={{ height: 35, width: 35 }} />
                          </div>
                        </div>


                        <div style={{ height: 40, width: 40, padding: 2 }}>
                          <div style={{ borderRadius: 50 }}>
                            <img src={require(`./images/minion_of_mordred2.png`)} style={{ height: 35, width: 35 }} />
                          </div>
                        </div>

                        <div style={{ height: 40, width: 40, padding: 2 }}>
                          <div style={{ borderRadius: 50 }}>
                            <img src={require(`./images/minion_of_mordred3.png`)} style={{ height: 35, width: 35 }} />
                          </div>
                        </div>

                        <div style={{ height: 40, width: 40, padding: 2 }}>
                          <div style={{ borderRadius: 50 }}>
                            <img src={require(`./images/minion_of_mordred4.png`)} style={{ height: 35, width: 35 }} />
                          </div>
                        </div>

                      </div>
                    </TabPane>

                    <TabPane tabId={"iconTabs_minion_of_mordred2"}>
                      <small className="font-weight-bold">
                        Minion of mordred
                      </small>
                      <p className="description">
                        See all evil members except Oberon
                    </p>


                      <div class="row" style={{ marginLeft: 0, marginRight: 0 }}>
                        <div style={{ height: 40, width: 40, padding: 2 }}>
                          <div style={{ borderRadius: 50 }}>
                            <img src={require(`./images/mordred.png`)} style={{ height: 35, width: 35 }} />
                          </div>
                        </div>

                        <div style={{ height: 40, width: 40, padding: 2 }}>
                          <div style={{ borderRadius: 50 }}>
                            <img src={require(`./images/assassin.png`)} style={{ height: 35, width: 35 }} />
                          </div>
                        </div>

                        <div style={{ height: 40, width: 40, padding: 2 }}>
                          <div style={{ borderRadius: 50 }}>
                            <img src={require(`./images/morgana.png`)} style={{ height: 35, width: 35 }} />
                          </div>
                        </div>



                        <div style={{ height: 40, width: 40, padding: 2 }}>
                          <div style={{ borderRadius: 50 }}>
                            <img src={require(`./images/minion_of_mordred1.png`)} style={{ height: 35, width: 35 }} />
                          </div>
                        </div>


                        <div style={{ height: 40, width: 40, padding: 2 }}>
                          <div style={{ borderRadius: 50 }}>
                            <img src={require(`./images/minion_of_mordred3.png`)} style={{ height: 35, width: 35 }} />
                          </div>
                        </div>

                        <div style={{ height: 40, width: 40, padding: 2 }}>
                          <div style={{ borderRadius: 50 }}>
                            <img src={require(`./images/minion_of_mordred4.png`)} style={{ height: 35, width: 35 }} />
                          </div>
                        </div>

                      </div>
                    </TabPane>

                    <TabPane tabId={"iconTabs_minion_of_mordred3"}>
                      <small className="font-weight-bold">
                        Minion of mordred
                      </small>
                      <p className="description">
                        See all evil members except Oberon
                    </p>


                      <div class="row" style={{ marginLeft: 0, marginRight: 0 }}>
                        <div style={{ height: 40, width: 40, padding: 2 }}>
                          <div style={{ borderRadius: 50 }}>
                            <img src={require(`./images/mordred.png`)} style={{ height: 35, width: 35 }} />
                          </div>
                        </div>

                        <div style={{ height: 40, width: 40, padding: 2 }}>
                          <div style={{ borderRadius: 50 }}>
                            <img src={require(`./images/assassin.png`)} style={{ height: 35, width: 35 }} />
                          </div>
                        </div>

                        <div style={{ height: 40, width: 40, padding: 2 }}>
                          <div style={{ borderRadius: 50 }}>
                            <img src={require(`./images/morgana.png`)} style={{ height: 35, width: 35 }} />
                          </div>
                        </div>



                        <div style={{ height: 40, width: 40, padding: 2 }}>
                          <div style={{ borderRadius: 50 }}>
                            <img src={require(`./images/minion_of_mordred1.png`)} style={{ height: 35, width: 35 }} />
                          </div>
                        </div>

                        <div style={{ height: 40, width: 40, padding: 2 }}>
                          <div style={{ borderRadius: 50 }}>
                            <img src={require(`./images/minion_of_mordred2.png`)} style={{ height: 35, width: 35 }} />
                          </div>
                        </div>



                        <div style={{ height: 40, width: 40, padding: 2 }}>
                          <div style={{ borderRadius: 50 }}>
                            <img src={require(`./images/minion_of_mordred4.png`)} style={{ height: 35, width: 35 }} />
                          </div>
                        </div>

                      </div>
                    </TabPane>

                    <TabPane tabId={"iconTabs_minion_of_mordred4"}>
                      <small className="font-weight-bold">
                        Minion of mordred
                      </small>
                      <p className="description">
                        See all evil members except Oberon
                    </p>


                      <div class="row" style={{ marginLeft: 0, marginRight: 0 }}>
                        <div style={{ height: 40, width: 40, padding: 2 }}>
                          <div style={{ borderRadius: 50 }}>
                            <img src={require(`./images/mordred.png`)} style={{ height: 35, width: 35 }} />
                          </div>
                        </div>

                        <div style={{ height: 40, width: 40, padding: 2 }}>
                          <div style={{ borderRadius: 50 }}>
                            <img src={require(`./images/assassin.png`)} style={{ height: 35, width: 35 }} />
                          </div>
                        </div>

                        <div style={{ height: 40, width: 40, padding: 2 }}>
                          <div style={{ borderRadius: 50 }}>
                            <img src={require(`./images/morgana.png`)} style={{ height: 35, width: 35 }} />
                          </div>
                        </div>



                        <div style={{ height: 40, width: 40, padding: 2 }}>
                          <div style={{ borderRadius: 50 }}>
                            <img src={require(`./images/minion_of_mordred1.png`)} style={{ height: 35, width: 35 }} />
                          </div>
                        </div>

                        <div style={{ height: 40, width: 40, padding: 2 }}>
                          <div style={{ borderRadius: 50 }}>
                            <img src={require(`./images/minion_of_mordred2.png`)} style={{ height: 35, width: 35 }} />
                          </div>
                        </div>

                        <div style={{ height: 40, width: 40, padding: 2 }}>
                          <div style={{ borderRadius: 50 }}>
                            <img src={require(`./images/minion_of_mordred3.png`)} style={{ height: 35, width: 35 }} />
                          </div>
                        </div>



                      </div>
                    </TabPane>

                    <TabPane tabId={"iconTabs_oberon"}>
                      <small className="font-weight-bold">
                        Oberon
                      </small>
                      <p className="description">
                        Can't See any one
                    </p>


                      <div class="row" style={{ marginLeft: 0, marginRight: 0 }}>


                      </div>
                    </TabPane>



                  </TabContent>
                </CardBody>
              </Card>

              <div style={{ height: 20 }}>

              </div>

              <small className="font-weight-bold mr-1">
                When Playing
                      </small>
              <p className="description">
                <u>To select the quest</u>
              </p>
              <p className="description">
                When you are the leader (with crest) you can click on the quest indicator on the table to select the quest you would like to compete, and then select your team
                </p >
              <p className="description">

                <u>
                  Lady of the lake Token
                </u>
              </p>
              <p className="description">
                Lady of the lake will appear as lake token on profile image at the end of 2nd, 3rd and 4th Quest. When lady of the lake point at a person, the loyalty of that person will be revealed.

                Lady of the lake cannot be used of previous lady of the lake token holders
                    </p>
            </Col>




          </div>
        </SlidingPanel>





        <SlidingPanel
          type={'right'}
          isOpen={this.state.siderpanel}
          size={this.state.width > 845 ? 20 : (this.state.width > 500 ? 40 : 60)}
          backdropClicked={() => this.setState({ siderpanel: false })}
        >
          <div style={{ backgroundColor: 'white', height: '100%' }}>
            <div class="list-group">

              <a href="#" class="list-group-item list-group-item-action">Help</a>
              <a href="#" class="list-group-item list-group-item-action">About</a>
              <a onClick={e => this.logout()} class="list-group-item list-group-item-action ">Log Out</a>

            </div>



          </div>
        </SlidingPanel>



        <div class='main col-xl' style={{backgroundImage:`url(${background_image})`}}>


          <Alert style={{ left: 0, position: 'fixed', width: '100%', zIndex: 25, display: this.state.Erroris ? 'flex' : 'none' }} color="danger">
            {this.state.Error.error}
          </Alert>

          <Alert style={{ left: 0, position: 'fixed', width: '100%', zIndex: 25, display: this.state.alertmsg ? 'flex' : 'none' }} color="success">
            {this.state.msg}
          </Alert>




          <div class="row main-row">
            <div class='col-sm-12 row position-fixed align-items-center' style={{ zIndex: 18 }}>

              <div class="row col-sm-2 align-items-center" sm="2">
                <p class="normal_text">
                  Connection
                </p>
                <div style={{ backgroundColor: this.state.connectionStatus ? 'green' : 'red' }} id="connection_ball">

                </div>
              </div>

              <div class="row col-sm-8 " sm="2">

                <div class="col-sm-8 position-fixed text-center center_button_div" sm="8" style={{ height: 1 }}>

                  <div class="row text-center justify-content-center" style={{ zIndex: 18, height: 1 }}>
                    <p class="url_link_test">
                      {"Room id: " + this.state.roomid}
                    </p>

                    <p class="url_link_test" style={{ paddingLeft: 5, paddingRight: 5 }}>   |    </p>
                    <p onClick={e => this.copyToClipboard()} class="url_link_test" style={{ cursor: 'pointer', textDecoration: 'underline' }} >
                      Copy Game Url
                    </p>
                  </div>



                  <Button className="hide_show_button" onClick={e => this.setState({ hideshow: !this.state.hideshow })} size="sm" type="button" style={{ top: this.state.height - 50 }}>
                    {this.state.hideshow ? "Hide" : "Show"}
                  </Button>


                </div>


              </div>



            </div>




            <div class="col-sm-2" sm="2">

            </div>


            <div class="col-sm-8 text-center" sm="8" style={{ zIndex: 13 }}>

              <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 100 100" width="100%" height={this.state.height - 30}>
                <defs>
                  {/*For main table image*/}
                  <filter id="imageblend" primitiveUnits="objectBoundingBox">
                    <feImage href={table} preserveAspectRatio="none" x="0" y="0" height="100%" width="100%" />
                    <feBlend mode="screen" in2="SourceGraphic" />
                    <feComposite operator="in" in2="SourceGraphic" />
                  </filter>

                  {/*For unknown person image*/}
                  <filter id="unknown" primitiveUnits="objectBoundingBox">
                    <feImage href={unknown} preserveAspectRatio="none" x="0" y="0" height="100%" width="100%" />
                    <feBlend mode="screen" in2="SourceGraphic" />
                    <feComposite operator="in" in2="SourceGraphic" />
                  </filter>
                  {/*For merlin image*/}
                  <filter id="assassin" primitiveUnits="objectBoundingBox">
                    <feImage href={assassin} preserveAspectRatio="none" x="0" y="0" height="100%" width="100%" />
                    <feBlend mode="screen" in2="SourceGraphic" />
                    <feComposite operator="in" in2="SourceGraphic" />
                  </filter>

                  {/*For merlin image*/}
                  <filter id="merlin" primitiveUnits="objectBoundingBox">
                    <feImage href={merlin} preserveAspectRatio="none" x="0" y="0" height="100%" width="100%" />
                    <feBlend mode="screen" in2="SourceGraphic" />
                    <feComposite operator="in" in2="SourceGraphic" />
                  </filter>

                  {/*For merlin image*/}
                  <filter id="minion_of_mordred1" primitiveUnits="objectBoundingBox">
                    <feImage href={minion_of_mordred1} preserveAspectRatio="none" x="0" y="0" height="100%" width="100%" />
                    <feBlend mode="screen" in2="SourceGraphic" />
                    <feComposite operator="in" in2="SourceGraphic" />
                  </filter>
                  {/*For merlin image*/}
                  <filter id="minion_of_mordred2" primitiveUnits="objectBoundingBox">
                    <feImage href={minion_of_mordred2} preserveAspectRatio="none" x="0" y="0" height="100%" width="100%" />
                    <feBlend mode="screen" in2="SourceGraphic" />
                    <feComposite operator="in" in2="SourceGraphic" />
                  </filter>
                  {/*For merlin image*/}
                  <filter id="minion_of_mordred3" primitiveUnits="objectBoundingBox">
                    <feImage href={minion_of_mordred3} preserveAspectRatio="none" x="0" y="0" height="100%" width="100%" />
                    <feBlend mode="screen" in2="SourceGraphic" />
                    <feComposite operator="in" in2="SourceGraphic" />
                  </filter>
                  {/*For merlin image*/}
                  <filter id="minion_of_mordred4" primitiveUnits="objectBoundingBox">
                    <feImage href={minion_of_mordred4} preserveAspectRatio="none" x="0" y="0" height="100%" width="100%" />
                    <feBlend mode="screen" in2="SourceGraphic" />
                    <feComposite operator="in" in2="SourceGraphic" />
                  </filter>
                  {/*For merlin image*/}
                  <filter id="mordred" primitiveUnits="objectBoundingBox">
                    <feImage href={mordred} preserveAspectRatio="none" x="0" y="0" height="100%" width="100%" />
                    <feBlend mode="screen" in2="SourceGraphic" />
                    <feComposite operator="in" in2="SourceGraphic" />
                  </filter>
                  {/*For merlin image*/}
                  <filter id="morgana" primitiveUnits="objectBoundingBox">
                    <feImage href={morgana} preserveAspectRatio="none" x="0" y="0" height="100%" width="100%" />
                    <feBlend mode="screen" in2="SourceGraphic" />
                    <feComposite operator="in" in2="SourceGraphic" />
                  </filter>
                  {/*For merlin image*/}
                  <filter id="oberon" primitiveUnits="objectBoundingBox">
                    <feImage href={oberon} preserveAspectRatio="none" x="0" y="0" height="100%" width="100%" />
                    <feBlend mode="screen" in2="SourceGraphic" />
                    <feComposite operator="in" in2="SourceGraphic" />
                  </filter>
                  {/*For merlin image*/}
                  <filter id="percival" primitiveUnits="objectBoundingBox">
                    <feImage href={percival} preserveAspectRatio="none" x="0" y="0" height="100%" width="100%" />
                    <feBlend mode="screen" in2="SourceGraphic" />
                    <feComposite operator="in" in2="SourceGraphic" />
                  </filter>
                  {/*For merlin image*/}
                  <filter id="servent1" primitiveUnits="objectBoundingBox">
                    <feImage href={servent1} preserveAspectRatio="none" x="0" y="0" height="100%" width="100%" />
                    <feBlend mode="screen" in2="SourceGraphic" />
                    <feComposite operator="in" in2="SourceGraphic" />
                  </filter>
                  {/*For merlin image*/}
                  <filter id="servent2" primitiveUnits="objectBoundingBox">
                    <feImage href={servent2} preserveAspectRatio="none" x="0" y="0" height="100%" width="100%" />
                    <feBlend mode="screen" in2="SourceGraphic" />
                    <feComposite operator="in" in2="SourceGraphic" />
                  </filter>
                  {/*For merlin image*/}
                  <filter id="servent3" primitiveUnits="objectBoundingBox">
                    <feImage href={servent3} preserveAspectRatio="none" x="0" y="0" height="100%" width="100%" />
                    <feBlend mode="screen" in2="SourceGraphic" />
                    <feComposite operator="in" in2="SourceGraphic" />
                  </filter>
                  {/*For merlin image*/}
                  <filter id="servent4" primitiveUnits="objectBoundingBox">
                    <feImage href={servent4} preserveAspectRatio="none" x="0" y="0" height="100%" width="100%" />
                    <feBlend mode="screen" in2="SourceGraphic" />
                    <feComposite operator="in" in2="SourceGraphic" />
                  </filter>
                  {/*For merlin image*/}
                  <filter id="servent5" primitiveUnits="objectBoundingBox">
                    <feImage href={servent5} preserveAspectRatio="none" x="0" y="0" height="100%" width="100%" />
                    <feBlend mode="screen" in2="SourceGraphic" />
                    <feComposite operator="in" in2="SourceGraphic" />
                  </filter>
                  {/*For merlin image*/}
                  <filter id="servent6" primitiveUnits="objectBoundingBox">
                    <feImage href={servent6} preserveAspectRatio="none" x="0" y="0" height="100%" width="100%" />
                    <feBlend mode="screen" in2="SourceGraphic" />
                    <feComposite operator="in" in2="SourceGraphic" />
                  </filter>
                  <filter id="half" primitiveUnits="objectBoundingBox">
                    <feImage href={half} preserveAspectRatio="none" x="0" y="0" height="100%" width="100%" />
                    <feBlend mode="screen" in2="SourceGraphic" />
                    <feComposite operator="in" in2="SourceGraphic" />
                  </filter>






                  {/*For circle on table image*/}
                  <filter id="desk_button" primitiveUnits="objectBoundingBox">
                    <feImage href={desk_buton} preserveAspectRatio="none" x="0" y="0" height="100%" width="100%" />
                    <feBlend mode="screen" in2="SourceGraphic" />
                    <feComposite operator="in" in2="SourceGraphic" />
                  </filter>

                  {/*For good circle on table image*/}
                  <filter id="desk_button_good" primitiveUnits="objectBoundingBox">
                    <feImage href={win_good} preserveAspectRatio="none" x="0" y="0" height="100%" width="100%" />
                    <feBlend mode="screen" in2="SourceGraphic" />
                    <feComposite operator="in" in2="SourceGraphic" />
                  </filter>

                  {/*For evil circle on table image*/}
                  <filter id="desk_button_evil" primitiveUnits="objectBoundingBox">
                    <feImage href={win_evil} preserveAspectRatio="none" x="0" y="0" height="100%" width="100%" />
                    <feBlend mode="screen" in2="SourceGraphic" />
                    <feComposite operator="in" in2="SourceGraphic" />
                  </filter>



                  {/*For name borad*/}
                  <filter id="name_borad" primitiveUnits="objectBoundingBox">
                    <feImage href={name_borad} preserveAspectRatio="none" x="0" y="0" height="100%" width="100%" />
                    <feBlend mode="screen" in2="SourceGraphic" />
                    <feComposite operator="in" in2="SourceGraphic" />
                  </filter>

                  <path id="text_0_path" d="M 100 150 A 100 100 0 1 1 300 150" />


                </defs>





                {/*Main table image*/}
                {/*} <circle cx={this.state.Originx} cy={this.state.Originy} r={this.state.radius} opacity="1" filter="url(#imageblend)" fill="black" fill-opacity="0.9"/>*/}
                <image href={table} x={this.state.Originx - this.state.radius} y={this.state.Originy - this.state.radius} height={2 * this.state.radius} width={2 * this.state.radius} />

                {/*For  number of persons section rounds */}


                {/*For success fail count*/}

                <text class="successFailText" x={this.state.Originx - 22.5} y={this.state.Originy - 11} fill="#e83e38" dy=".3em">
                  {this.state.questcombination[0].fail_count}
                </text>
                <text class="successFailText" x={this.state.Originx - 20.5} y={this.state.Originy - 11} fill={svgtheme.LELEL_BUTTON_FONT_COLOR} dy=".3em">
                  {this.state.questcombination[0].total !== '' ? "/ " : ''} {this.state.questcombination[0].total}
                </text>

                <text onClick={e => this.setquest(1)} class="Main_number_text" x={this.state.Originx - 20} y={this.state.Originy - 5} text-anchor="middle" fill={svgtheme.DESK_BUTTON_FONT_COLOR} font-size={svgtheme.DESK_BUTTON_FONT_SIZE} font-family={svgtheme.DESK_BUTTON_FONT_FAMILY} dy=".3em">
                  {this.state.requered_selectors_in_game[0].count}
                </text>
                <circle cx={this.state.Originx - 20} cy={this.state.Originy - 4.9} r='4.6' stroke="#0088b5" stroke-width="0.6" fill-opacity="0" style={{ display: this.state.currunt_quest === 1 ? "flex" : "none" }} />
                <image class="quest" onClick={e => this.setquest(1)} href={this.state.quests[0] === "good" ? win_good : (this.state.quests[0] === "evil" ? win_evil : desk_buton)} x={this.state.Originx - 25} y={this.state.Originy - 10} height="10" width="10" />


                {/*For success fail count*/}

                <text class="successFailText" x={this.state.Originx - 12.5} y={this.state.Originy - 11} fill="#e83e38" dy=".3em">
                  {this.state.questcombination[1].fail_count}
                </text>
                <text class="successFailText" x={this.state.Originx - 10.5} y={this.state.Originy - 11} fill={svgtheme.LELEL_BUTTON_FONT_COLOR} dy=".3em">
                  {this.state.questcombination[1].total !== '' ? "/" : ''} {this.state.questcombination[1].total}
                </text>


                <text onClick={e => this.setquest(2)} class="Main_number_text" x={this.state.Originx - 10} y={this.state.Originy - 5} text-anchor="middle" fill={svgtheme.DESK_BUTTON_FONT_COLOR} font-size={svgtheme.DESK_BUTTON_FONT_SIZE} font-family={svgtheme.DESK_BUTTON_FONT_FAMILY} dy=".3em">
                  {this.state.requered_selectors_in_game[1].count}
                </text>
                <circle cx={this.state.Originx - 10} cy={this.state.Originy - 4.9} r='4.6' stroke="#0088b5" stroke-width="0.6" fill-opacity="0" style={{ display: this.state.currunt_quest === 2 ? "flex" : "none" }} />
                <image class="quest" onClick={e => this.setquest(2)} href={this.state.quests[1] === "good" ? win_good : (this.state.quests[1] === "evil" ? win_evil : desk_buton)} x={this.state.Originx - 15} y={this.state.Originy - 10} height="10" width="10" />



                {/*For success fail count*/}

                <text class="successFailText" x={this.state.Originx - 2.5} y={this.state.Originy - 11} fill="#e83e38" dy=".3em">
                  {this.state.questcombination[2].fail_count}
                </text>
                <text class="successFailText" x={this.state.Originx - 0.5} y={this.state.Originy - 11} fill={svgtheme.LELEL_BUTTON_FONT_COLOR} dy=".3em">
                  {this.state.questcombination[2].total !== '' ? "/" : ''} {this.state.questcombination[2].total}
                </text>

                <text onClick={e => this.setquest(3)} class="Main_number_text" x={this.state.Originx} y={this.state.Originy - 5} text-anchor="middle" fill={svgtheme.DESK_BUTTON_FONT_COLOR} font-size={svgtheme.DESK_BUTTON_FONT_SIZE} font-family={svgtheme.DESK_BUTTON_FONT_FAMILY} dy=".3em">
                  {this.state.requered_selectors_in_game[2].count}
                </text>
                <circle cx={this.state.Originx} cy={this.state.Originy - 4.9} r='4.6' stroke="#0088b5" stroke-width="0.6" fill-opacity="0" style={{ display: this.state.currunt_quest === 3 ? "flex" : "none" }} />
                <image class="quest" onClick={e => this.setquest(3)} href={this.state.quests[2] === "good" ? win_good : (this.state.quests[2] === "evil" ? win_evil : desk_buton)} x={this.state.Originx - 5} y={this.state.Originy - 10} height="10" width="10" />


                {/*For success fail count*/}

                <text class="successFailText" x={this.state.Originx + 7.5} y={this.state.Originy - 11} fill="#e83e38" dy=".3em">
                  {this.state.questcombination[3].fail_count}
                </text>
                <text class="successFailText" x={this.state.Originx + 9.5} y={this.state.Originy - 11} fill={svgtheme.LELEL_BUTTON_FONT_COLOR} dy=".3em">
                  {this.state.questcombination[3].total !== '' ? "/" : ''} {this.state.questcombination[3].total}
                </text>

                <text onClick={e => this.setquest(4)} class="Main_number_text" x={this.state.Originx + 10} y={this.state.Originy - 5} text-anchor="middle" fill={svgtheme.DESK_BUTTON_FONT_COLOR} font-size={svgtheme.DESK_BUTTON_FONT_SIZE} font-family={svgtheme.DESK_BUTTON_FONT_FAMILY} dy=".3em">
                  {this.state.requered_selectors_in_game[3].count}
                </text>
                <circle cx={this.state.Originx + 10} cy={this.state.Originy - 4.9} r='4.6' stroke="#0088b5" stroke-width="0.6" fill-opacity="0" style={{ display: this.state.currunt_quest === 4 ? "flex" : "none" }} />
                <image class="quest" onClick={e => this.setquest(4)} href={this.state.quests[3] === "good" ? win_good : (this.state.quests[3] === "evil" ? win_evil : desk_buton)} x={this.state.Originx + 5} y={this.state.Originy - 10} height="10" width="10" />
                <text onClick={e => this.setquest(4)} x={this.state.Originx + 10.4} y={this.state.Originy + 1} text-anchor="middle" fill={svgtheme.DESK_BUTTON_FONT_COLOR} font-size="1.5" dy=".3em" style={{ display: this.state.requered_selectors_in_game[3].evil_count === 2 ? 'flex' : 'none' }}>
                  fail req {this.state.requered_selectors_in_game[3].evil_count}

                </text>



                {/*For success fail count*/}

                <text class="successFailText" x={this.state.Originx + 17.5} y={this.state.Originy - 11} fill="#e83e38" dy=".3em">
                  {this.state.questcombination[4].fail_count}
                </text>
                <text class="successFailText" x={this.state.Originx + 19.5} y={this.state.Originy - 11} fill={svgtheme.LELEL_BUTTON_FONT_COLOR} dy=".3em">
                  {this.state.questcombination[4].total !== '' ? "/" : ''} {this.state.questcombination[4].total}
                </text>

                <text onClick={e => this.setquest(5)} class="Main_number_text" x={this.state.Originx + 20} y={this.state.Originy - 5} text-anchor="middle" fill={svgtheme.DESK_BUTTON_FONT_COLOR} font-size={svgtheme.DESK_BUTTON_FONT_SIZE} font-family={svgtheme.DESK_BUTTON_FONT_FAMILY} dy=".3em">
                  {this.state.requered_selectors_in_game[4].count}
                </text>
                <circle cx={this.state.Originx + 20} cy={this.state.Originy - 4.9} r='4.6' stroke="#0088b5" stroke-width="0.6" fill-opacity="0" style={{ display: this.state.currunt_quest === 5 ? "flex" : "none" }} />
                <image class="quest" onClick={e => this.setquest(5)} href={this.state.quests[4] === "good" ? win_good : (this.state.quests[4] === "evil" ? win_evil : desk_buton)} x={this.state.Originx + 15} y={this.state.Originy - 10} height="10" width="10" />




                {/*For Levels round and text */}
                <text class="level_text" x={this.state.Originx - 14} y={this.state.Originy + 15} text-anchor="middle" fill={svgtheme.LELEL_BUTTON_FONT_COLOR} font-size={svgtheme.LELEL_BUTTON_FONT_SIZE} font-family={svgtheme.LELEL_BUTTON_FONT_FAMILY} dy=".3em">
                  1
                </text>
                {/*<circle cx={this.state.Originx - 14} cy={this.state.Originy + 15} r='3.5' opacity="1" filter="url(#desk_button)" fill="#522916" fill-opacity="0.5" />*/}
                <image href={this.state.rejected_count === 0 ? current_quest_button : desk_buton} x={this.state.Originx - 17.5} y={this.state.Originy + 11.5} height="7" width="7" />




                <text class="level_text" x={this.state.Originx - 7} y={this.state.Originy + 15} text-anchor="middle" fill={svgtheme.LELEL_BUTTON_FONT_COLOR} font-size={svgtheme.LELEL_BUTTON_FONT_SIZE} dy=".3em">
                  2
                </text>

                {/*<circle cx={this.state.Originx - 7} cy={this.state.Originy + 15} r='3.5' opacity="1" filter="url(#desk_button)" fill="#522916" fill-opacity="0.5" />*/}
                <image href={this.state.rejected_count === 1 ? current_quest_button : desk_buton} x={this.state.Originx - 10.5} y={this.state.Originy + 11.5} height="7" width="7" />


                <text class="level_text" x={this.state.Originx} y={this.state.Originy + 15} text-anchor="middle" fill={svgtheme.LELEL_BUTTON_FONT_COLOR} font-size={svgtheme.LELEL_BUTTON_FONT_SIZE} font-family={svgtheme.LELEL_BUTTON_FONT_FAMILY} dy=".3em">
                  3
                </text>
                {/*<circle cx={this.state.Originx} cy={this.state.Originy + 15} r='3.5' opacity="1" filter="url(#desk_button)" fill="#522916" fill-opacity="0.5" />*/}
                <image href={this.state.rejected_count === 2 ? current_quest_button : desk_buton} x={this.state.Originx - 3.5} y={this.state.Originy + 11.5} height="7" width="7" />



                <text class="level_text" x={this.state.Originx + 7} y={this.state.Originy + 15} text-anchor="middle" fill={svgtheme.LELEL_BUTTON_FONT_COLOR} font-size={svgtheme.LELEL_BUTTON_FONT_SIZE} font-family={svgtheme.LELEL_BUTTON_FONT_FAMILY} dy=".3em">
                  4
                </text>
                {/*<circle cx={this.state.Originx + 7} cy={this.state.Originy + 15} r='3.5' opacity="1" filter="url(#desk_button)" fill="#522916" fill-opacity="0.5" />*/}
                <image href={this.state.rejected_count === 3 ? current_quest_button : desk_buton} x={this.state.Originx + 3.5} y={this.state.Originy + 11.5} height="7" width="7" />


                <text class="level_text" x={this.state.Originx + 14} y={this.state.Originy + 15} text-anchor="middle" fill={svgtheme.LELEL_BUTTON_FONT_COLOR} font-size={svgtheme.LELEL_BUTTON_FONT_SIZE} font-family={svgtheme.LELEL_BUTTON_FONT_FAMILY} dy=".3em">
                  5
                </text>
                <circle cx={this.state.Originx + 14} cy={this.state.Originy + 15} r='3.5' opacity="1" filter="url(#desk_button)" fill="red" fill-opacity="0.5" />
                <image href={this.state.rejected_count === 4 ? current_quest_button : desk_buton} x={this.state.Originx + 10.5} y={this.state.Originy + 11.5} height="7" width="7" />


                {/*For status text*/}

                <text class="status_text" x={this.state.Originx - 9} y={this.state.Originy + 22} text-anchor="middle" fill={svgtheme.LELEL_BUTTON_FONT_COLOR} font-size={svgtheme.LELEL_BUTTON_FONT_SIZE} font-family={svgtheme.LELEL_BUTTON_FONT_FAMILY} dy=".3em">
                  Status: {this.state.startgame ? "STARTED" : "PENDING"}
                </text>

                {/*For good vs bad text*/}

                <text class="good_number" x={this.state.Originx + 9} y={this.state.Originy + 22} text-anchor="middle" fill="#42b840" font-size={svgtheme.LELEL_BUTTON_FONT_SIZE} font-family={svgtheme.LELEL_BUTTON_FONT_FAMILY} dy=".3em">
                  {this.state.goodvsevil.good}
                </text>
                <text class="status_text" x={this.state.Originx + 11.5} y={this.state.Originy + 22.1} text-anchor="middle" fill={svgtheme.LELEL_BUTTON_FONT_COLOR} font-size={svgtheme.LELEL_BUTTON_FONT_SIZE} font-family={svgtheme.LELEL_BUTTON_FONT_FAMILY} dy=".3em">
                  vs
                </text>
                <text class="evil_number" x={this.state.Originx + 14} y={this.state.Originy + 22} text-anchor="middle" fill="#e83e38" font-size={svgtheme.LELEL_BUTTON_FONT_SIZE} font-family={svgtheme.LELEL_BUTTON_FONT_FAMILY} dy=".3em">
                  {this.state.goodvsevil.evil}
                </text>




                {/*For success fail count*/}

                <text class="successFailText" x={this.state.Originx - 4.5} y={this.state.Originy - 15} fill="#e83e38" dy=".3em">
                  {this.state.failscount}
                </text>
                <text class="successFailText" x={this.state.Originx - 2.5} y={this.state.Originy - 15} fill={svgtheme.LELEL_BUTTON_FONT_COLOR} dy=".3em">
                  {this.state.totalcount}
                </text>







                {/*For persons round,name */}
                {this.state.users.map((value, index) => {
                  var x = this.generateNode(index, "x");
                  var y = this.generateNode(index, "y");

                  //var image="url(#"+value.character+")";
                  var image = "url(#unknown)";
                  var im = "unknown";
                  if (this.state.hideshow || this.state.endgame) {
                    image = "url(#" + value.character + ")";
                    im = value.character;
                  }

                 
                  return (
                    <g>

                      <defs>
                        <clipPath id={"clip" + index} clipPathUnits="userSpaceOnUse">
                          <rect x="10" x={x - 8} y={y + 7} width="16" height="2.5" />
                        </clipPath>




                      </defs>

                      {/*For persons round */}
                      <circle class='person' onClick={e => this.selecting(value.email)} opacity={value.accept_arry == "unknown" ? "1" : "1"} filter={image} key={index} cx={x} cy={y} r={6} stroke={value.type == "evil" ? (this.state.hideshow ? "red" : "") : ""} stroke-width="0.5" />

                      <circle class='person' onClick={e => this.selecting(value.email)} fillOpacity={0} key={index} cx={x} cy={y} r={6} stroke={value.lady_select != "unknown" ? (this.state.hideshow ? (value.lady_select == "evil" ? "red" : (value.lady_select == "good" ? "green" : "")) : "none") : ""} stroke-width="0.5" />

                      <image onClick={e => this.selecting(value.email)} href={require(`./images/lady_selected.png`)} x={x - 8.4} y={y - 8.4} height="16" width="16" style={{ opacity: value.lady_select == true ? "1" : "0" }} />

                      <image onClick={e => this.selecting(value.email)} href={require(`./images/${im}.png`)} x={x - 14} y={y - 6} height="12" width="28" style={{ opacity: value.accept_arry == "unknown" ? "1" : "1" }} />

                      {/*For lake lady*/}
                      <image onClick={e => this.selecting(value.email)} href={cloak} x={x - 14} y={y - 4} height="12" width="28" style={{ display: (value.lady_lake == true) ? 'flex' : 'none' }} />

                      {/*For name */}
                      <rect x={x - 7.52} y={y + 7.1} onClick={e => this.selecting(value.email)} width="15" height="2.2" rx="1" opacity="1" filter="url(#name_borad)" fill="black" fill-opacity="1" />
                      <image onClick={e => this.selecting(value.email)} href={require(`./images/name_borad.png`)} x={x - 8} y={y + 7.1} height="2.5" width="16" />

                      <text class="person_name" onClick={e => this.selecting(value.email)} x={x} y={y + 9} width="16" height="2.5" text-anchor="middle" clip-path={"url(#clip" + index + ")"} font-size="2" fill={value.accept_arry == "unknown"?"gray":"white"}>{value.name}</text>



                      {/*For sword*/}
                      <image id={"sword" + index} onClick={e => this.selecting(value.email)} href={sword} x={x - 8} y={y - 7} height="15" width="8" style={{ display: value.selectByLeader ? 'flex' : 'none' }} />

                      {/*For Shield*/}
                      <image id={"Shield" + index} onClick={e => this.selecting(value.email)} href={Shield} x={x + 2} y={y - 9} height="20" width="10" style={{ display: value.sheild ? 'flex' : 'none',opacity:value.successfails =="unknown" ? 0.5:1}} />

                      {/*For QuestSelecting*/}
                      <image id={"QuestSelecting" + index} onClick={e => this.selecting(value.email)} href={QuestSelecting} x={x - 6} y={y - 6} height="20" width="12" style={{ display: value.leader ? 'flex' : 'none' }} />

                      {/*For QuestSelecting*/}
                      <image id={"apprual" + index} onClick={e => this.selecting(value.email)} href={approved} x={x - 6} y={y - 6} height="3" width="3" style={{ display: value.aproval == true ? 'flex' : 'none' }} />
                      {/*For QuestSelecting*/}
                      <image id={"reject" + index} onClick={e => this.selecting(value.email)} href={rejected} x={x - 6} y={y - 6} height="3" width="3" style={{ display: value.aproval == false ? 'flex' : 'none' }} />

                      {/*For online*/}
                      <image id={"apprual" + index} onClick={e => this.selecting(value.email)} href={online_icon} x={x - 6} y={y + 4} height="2.5" width="2.5" style={{ display: value.onlinestatus == true ? 'flex' : 'none' }} />
                      {/*For offline*/}
                      <image id={"reject" + index} onClick={e => this.selecting(value.email)} href={ofline_icon} x={x - 6} y={y + 4} height="2.5" width="2.5" style={{ display: value.onlinestatus == false ? 'flex' : 'none' }} />


                     

                      {/*For merlin geuest*/}
                      <image onClick={e => this.selecting(value.email)} href={killer} x={x - 6.5} y={y -2} height="10" width="13" style={{ display: value.guestperson == true ? 'flex' : 'none' }} />

                      {/*For merlin*/}
                      <image onClick={e => this.selecting(value.email)} href={merlin_select} x={x - 7.2} y={y - 7} height="10" width="15" style={{ display: value.gestedMerlin == true ? 'flex' : 'none' }} />


                    </g>
                  )
                })}


                <g style={{ display: this.state.approvelbuttons ? "none" : "none" }}>
                  <rect x={this.state.Originx - 12} y={this.state.Originy + 3} onClick={e => alert("")} width="12" height="5" rx="1" opacity="1" fill="#00ad34" fill-opacity="1" />
                  <text class="person_name" onClick={e => alert("")} x={this.state.Originx - 6} y={this.state.Originy + 6} width="12" height="2.5" text-anchor="middle" font-size="2" fill="white">Approve</text>

                  <rect x={this.state.Originx + 1} y={this.state.Originy + 3} onClick={e => alert("")} width="12" height="5" rx="1" opacity="1" fill="#f53636" fill-opacity="1" />
                  <text class="person_name" onClick={e => alert("")} x={this.state.Originx + 7} y={this.state.Originy + 6} width="12" height="2.5" text-anchor="middle" font-size="2" fill="white">Unapprove</text>

                </g>



                <g onClick={e => this.submitselection()} style={{ display: (this.state.leader && this.state.selecting) ? "flex" : "none" }}>
                  <rect className="svg_button" x={this.state.Originx - 5} y={this.state.Originy + 3} width="10" height="5" rx="1" opacity="1" fill="#1c0b00" fill-opacity="0.8" stroke="#a8753e" stroke-width="0.3"/>
                  <text class="person_name svg_button" x={this.state.Originx} y={this.state.Originy + 6} width="16" height="2.5" text-anchor="middle" font-size="2" fill="white">Submit</text>
                </g>

                {/**For submit guested merlin */}
                <g onClick={e => this.submitGuestedMerlin()} style={{ display: (this.state.guessmrelin) ? "flex" : "none" }}>
                  <rect className="svg_button" x={this.state.Originx - 5} y={this.state.Originy + 3} width="10" height="5" rx="1" opacity="1" fill="#1c0b00" fill-opacity="0.8" stroke="#a8753e" stroke-width="0.3"/>
                  <text class="person_name svg_button" x={this.state.Originx} y={this.state.Originy + 6} width="16" height="2.5" text-anchor="middle" font-size="2" fill="white">Submit</text>
                </g>


                {/**For start game button */}
                <g onClick={e => this.startGame()} style={{ display: (!this.state.startgame && this.state.gamecreator) ? "flex" : "none" }}>
                  <rect className="svg_button" x={this.state.Originx - 5} y={this.state.Originy + 3} width="10" height="5" rx="1" opacity="1" fill="#1c0b00" fill-opacity="0.8" stroke="#a8753e" stroke-width="0.3"/>
                  <text class="person_name svg_button" x={this.state.Originx} y={this.state.Originy + 6} width="10" height="2.5" text-anchor="middle" font-size="2" fill="white">Start</text>

                </g>


              </svg>




            </div>
            <div class="col-sm-2" sm="2">

            </div>
          </div>






          {/*For create or join game pop up*/}
          <Modal
            className="modal-dialog-centered accept_reject_model"
            isOpen={this.state.joinOrcreate}
          //toggle={() => this.toggleModal("sittingModal")}

          >

            <div className="modal-body gamejoin_modal">


              <Input placeholder="Room ID" type="email" className="transparent_button"
                style={{ marginBottom: 20 }}
                value={this.state.roomid}
                onChange={e => this.setState({ roomid: e.target.value })}
              />


              <Button
                className="transparent_button"

                data-dismiss="modal"
                type="button"
                onClick={() => this.createGame()}
              >
                NewGame
            </Button>
              <Button
                className="transparent_button"
                type="button"
                onClick={() => this.joinToGame()}
              >
                Join
            </Button>

            </div>

          </Modal>



          {/*For initiate game pop up*/}
          <Modal
            className="modal-dialog-centered accept_reject_model"
            isOpen={this.state.initiateGameModal}
          //toggle={() => this.toggleModal("sittingModal")}

          >

            <div className="modal-body gamejoin_modal">


              <div className="custom-control text-left custom-control-alternative custom-checkbox mb-3">
                <input
                  className="custom-control-input"
                  id="check_morgana_percival"
                  type="checkbox"
                  onChange={e => this.handleChange("morgana_percival", e)}

                />
                <label className="custom-control-label textOn_wood" htmlFor="check_morgana_percival">
                  Morgana & Percival
          </label>
              </div>
              <div className="custom-control text-left custom-control-alternative custom-checkbox mb-3">
                <input
                  className="custom-control-input"
                  id="check_mordred"
                  type="checkbox"
                  onChange={e => this.handleChange("mordred", e)}

                />
                <label className="custom-control-label textOn_wood" htmlFor="check_mordred">
                  Mordred
          </label>
              </div>

              <div className="custom-control text-left custom-control-alternative custom-checkbox mb-3">
                <input
                  className="custom-control-input"
                  id="check_oberon"
                  type="checkbox"
                  onChange={e => this.handleChange("oberon", e)}

                />
                <label className="custom-control-label textOn_wood" htmlFor="check_oberon">
                  Oberon
          </label>
              </div>

              <hr style={{ backgroundColor: '#f5cc86' }} />

              <div className="custom-control text-left custom-control-alternative custom-checkbox mb-3">
                <input
                  className="custom-control-input"
                  id="check_anonymous_approve"
                  type="checkbox"
                  onChange={e => this.handleChange("anonymous_approve", e)}

                />
                <label className="custom-control-label textOn_wood" htmlFor="check_anonymous_approve">
                  Anonymous Approve / Rejects
          </label>
              </div>

              <div className="custom-control text-left custom-control-alternative custom-checkbox mb-3">
                <input
                  className="custom-control-input"
                  id="check_can_fail"
                  type="checkbox"
                  onChange={e => this.handleChange("goodGuysCanFailQuests", e)}

                />
                <label className="custom-control-label textOn_wood" htmlFor="check_can_fail">
                  Good Guys Can Fail Quests
          </label>
              </div>
              <div className="custom-control text-left custom-control-alternative custom-checkbox mb-3">
                <input
                  className="custom-control-input"
                  id="ladyOFTheLake"
                  type="checkbox"
                  onChange={e => this.handleChange("ladyOFTheLake", e)}

                />
                <label className="custom-control-label textOn_wood" htmlFor="ladyOFTheLake">
                  Lady of the Lake
          </label>
              </div>


              <Button
                className="transparent_button"

                data-dismiss="modal"
                type="button"
                onClick={() => this.initiateGame()}
              >
                Start Game
            </Button>
              <Button
                className="transparent_button"
                type="button"
                onClick={() => this.leaveGame()}
              >
                Leave Game
            </Button>

            </div>

          </Modal>





          {/*For Sitting or not pop up*/}
          <Modal
            className="modal-dialog-centered accept_reject_model"
            isOpen={this.state.sittingModal}
          //toggle={() => this.toggleModal("sittingModal")}

          >

            <div className="modal-body">
              <Button
                color="success"
                data-dismiss="modal"
                type="button"
                onClick={() => this.toggleModal("sittingModal")}
              >
                Sitting
            </Button>
              <Button color="danger" type="button">
                StandUp
            </Button>

            </div>

          </Modal>

          {/*For approve or not*/}
          <Modal
            className="modal-dialog-centered accept_reject_model"
            isOpen={this.state.approvelbuttons}

          //toggle={() => this.toggleModal("sittingModal")}

          >

            <div className="modal-body gamejoin_modal">
              <div class="row justify-content-center">
                <div className="approve_button" onClick={e => this.approve(true)} >
                </div>

                <div className="reject_button" onClick={e => this.approve(false)}>
                </div>
              </div>

            </div>

          </Modal>


          {/*For success or fail*/}
          <Modal
            className="modal-dialog-centered accept_reject_model"
            isOpen={this.state.successFailmodel}

          //toggle={() => this.toggleModal("sittingModal")}

          >

            <div className="modal-body gamejoin_modal">
              <div class="row justify-content-center">
                <div className="success_button" onClick={e => this.successOrfail(true)} >
                </div>

                <div className="fail_button" onClick={e => this.successOrfail(false)}>
                </div>
              </div>

            </div>

          </Modal>



          {/*For end game*/}
          <Modal
            className="modal-dialog-centered accept_reject_model"
            isOpen={this.state.win}

            toggle={() => this.state.winAndRequest ? this.setState({ win: true }) : this.setState({ win: false })}

          >

            <div className="modal-body gamejoin_modal">

              <img src={this.state.winby == 'evil' ? won_evil : won_good} alt="Smiley face" height="160" width="150" />

              <div class="row justify-content-center py-lg-1" >
                <text class="win_text" >
                  {this.state.winby == 'evil' ? "Minions of Mordred Won" : "King Arthur Won"}
                </text>
              </div>


              <div class="row justify-content-center" style={{ display: this.state.winAndRequest ? 'flex' : 'none' }}>
                <Button
                  className="transparent_button"

                  data-dismiss="modal"
                  type="button"
                  onClick={() => this.startNewGame()}
                >
                  New Game
            </Button>
                <Button
                  className="transparent_button"
                  type="button"
                  onClick={() => this.leaveGame()}
                >
                  Leave Game
            </Button>
              </div>

            </div>

          </Modal>


          {/*For response of lady*/}
          <Modal
            className="modal-dialog-centered accept_reject_model"
            isOpen={this.state.showtypeforlady}

            toggle={() => this.setState({ showtypeforlady: false })}

          >

            <div className="modal-body gamejoin_modal">

              <img src={this.state.requestettype == 'evil' ? win_evil : win_good} height="100" width="100" />

              <div class="row justify-content-center " >
                <text class="win_text" >
                  {this.state.requestettype}
                </text>
              </div>




            </div>

          </Modal>


          {/*For response of lady*/}
          <Modal
            className="modal-dialog-centered accept_reject_model"
            isOpen={this.state.requestladyModel}
          //isOpen={true}
          // toggle={() => this.setState({ requestladyModel: false })}

          >


            <div className="modal-body gamejoin_modal">

              <img src={lady_of_lake} alt="Smiley face" height="150" />



              <div class="row justify-content-center py-lg-5" >
                <text class="normal_text">
                  Do you select Lady of the Lake
                </text>
              </div>


              <div class="row justify-content-center" style={{ display: this.state.leader ? 'flex' : 'none' }}>
                <Button
                  className="transparent_button"

                  data-dismiss="modal"
                  type="button"
                  onClick={() => this.acceptlady(true)}
                >
                  YES
</Button>
                <Button
                  className="transparent_button"
                  type="button"
                  onClick={() => this.acceptlady(false)}
                >
                  NO
</Button>
              </div>

            </div>

          </Modal>



          <Modal
            className="modal-dialog-centered accept_reject_model"
            isOpen={this.state.errormodel}
            //isOpen={true}
            toggle={() => this.setState({ errormodel: false })}

          >


            <div className="modal-body gamejoin_modal">



              <div class="row justify-content-center py-lg-5" >
                <text class="normal_text">
                  {this.state.error_in_model}
                </text>
              </div>




            </div>

          </Modal>



        </div>
      </div>
    )
  }
}

Main.defaultProps = {
  n: 7
}
