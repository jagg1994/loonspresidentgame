import React from "react";
import ReactDOM from "react-dom";
import { BrowserRouter, Route, Switch, Redirect } from "react-router-dom";
//import cookie from 'react-cookies';

import "./assets/vendor/nucleo/css/nucleo.css";
import "./assets/vendor/font-awesome/css/font-awesome.min.css";
//import "assets/scss/argon-design-system-react.scss";
import "./assets/scss/argon-design-system-react.scss"
import './index.css';
import App from './Views/App/App';
import Login from './Views/Login/Login';
import Registration from './Views/Registration/Registration';


import * as serviceWorker from './serviceWorker';
/*var isAuthenticated=false; 
var name=cookie.load('name')
if(name!=null){
  isAuthenticated=true
}*/


ReactDOM.render(
  <BrowserRouter>
    <Switch>

      <Route path="/login" exact render={props => <Login {...props} />} />
      <Route path="/Registration" exact render={props => <Registration {...props} />} />
      <Route path="/game" exact render={props => <App {...props} />} />
      
      <Route path="/login/:roomId" exact render={props => <Login {...props} />} />
      <Route path="/Registration/:roomId" exact render={props => <Registration {...props} />} />
      <Route path="/game/:roomId" exact render={props => <App {...props} />} />
      
      <Redirect to="/game" />
      <Redirect to="/game/:roomId" />
    </Switch>
  </BrowserRouter>,
  document.getElementById('root')
);

/*
function PrivateRoute({ children, ...rest }) {
  return (
    <Route
      {...rest}
      render={({ location }) =>
       isAuthenticated ? (
          children
        ) : (
            <Redirect
              to={{
                pathname: "/login",
                state: { from: location }
              }}
            />
          )
      }
    />
  );
}
*/

//ReactDOM.render(<App />, document.getElementById('root'));
// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
